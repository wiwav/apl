#include "aPL.h"
#include <fftw3.h>
//using namespace aPL_nspc;

aPL::aPL()
{
    //BEGIN LOCAL THREADS
    //END LOCAL THREADS
    fs = 44100.0f;
}

aPL* aPL::in(aPL* unit)
{
    UNUSED(unit);
    return this;
}

aPL* aPL::in(aPL* unit1, aPL* unit2)
{
    UNUSED(unit1);
    UNUSED(unit2);
    return this;
}

aPL* aPL::in(aPL** units)
{
    UNUSED(units);
    return this;
}

aPL* aPL::in(int clock)
{
    UNUSED(clock);
    return this;
}

aPL* aPL::in(int clock, int device)
{
    UNUSED(clock);
    UNUSED(device);
    return this;
}

void aPL::in(double inValue)
{
    UNUSED(inValue);
}

void aPL::in(void* inArrayOrVectorValue)
{
    UNUSED(inArrayOrVectorValue);
}

aPL* aPL::out(/*out params*/)
{
    return this;
}

double aPL::outV()
{
    //one sample value in clock tact
    return 0.0;
}

void* aPL::outA()
{
    //out Array Or Vector Value
    return 0;
}

aPL* aPL::LFO(int numberLFO, float amount, float rate, int shape)
{
    lfo[numberLFO].amount = amount;
    lfo[numberLFO].rate = rate;
    lfo[numberLFO].shape = shape;
    return this;
}

double aPL::LFO(int numberLFO, int n, int N)
{

    switch(lfo[numberLFO].shape)
    {
        case SAW:
            return getPerfectSawSample(fs, lfo[numberLFO].amount, lfo[numberLFO].rate, 0.0, n, N);
        break;
        case SQUARE:
            return getPerfectSquareSample(fs, lfo[numberLFO].amount, lfo[numberLFO].rate, 0.0, n, N, BIPOLAR);
        break;
        case SIN:
            return getPerfectSineSample(fs, lfo[numberLFO].amount, lfo[numberLFO].rate, 0.0, n, N);
        break;
        case NOISE:
            return getNoiseSample(lfo[numberLFO].amount, BIPOLAR);
        break;
    }
    return 0.0;
}

aPL* aPL::EG()
{
    return this;
}

aPL* aPL::VCO()
{
    return this;
}

aPL* aPL::OSC(int numberOSC, float coarse, float fine, int shape, int mix)
{
    osc[numberOSC].coarse = coarse;
    osc[numberOSC].fine = fine;
    osc[numberOSC].shape = shape;
    osc[numberOSC].mix = mix/100.0;
    return this;
}

aPL* aPL::DCO()
{
    return this;
}

/*VCF's*/
aPL* aPL::LPF()
{
    return this;
}

aPL* aPL::HPF()
{
    return this;
}

aPL* aPL::BPF()
{
    return this;
}

aPL* aPL::VCA()
{
    return this;
}

aPL* aPL::FX()
{
    return this;
}

aPL* aPL::MUX()
{
    return this;
}

aPL::~aPL()
{
    if(rtAudio)
    {
        rtAudio->stopStream();
        rtAudio->closeStream();
        delete rtAudio;
    }
}

double aPL::windowFunction(int typeWindow, int n, int N)
{
    switch (typeWindow)
    {
        case HAMMING:
        {//Hamming window
            double a = 0.53836;
            double b = 0.46164;
            return a-b*cos((2*M_PI*n)/(N-1));
        }break;

        case HANNING:
        {//Hanning(Hann) window
            double a0 = 0.5229;
            return a0-((1-a0)*cos((2*M_PI*n)/(N-1)));
        }break;

        case BLACKMAN_HARRIS:
        {//Blackman-Harris window
            double a0 = 0.35875;
            double a1 = 0.48829;
            double a2 = 0.14128;
            double a3 = 0.01168;
            return a0-(a1*cos((2*M_PI*n)/(N-1)))+(a2*cos((4*M_PI*n)/(N-1)))-(a3*cos((6*M_PI*n)/(N-1)));
        }break;

        case BLACKMAN:
        {//Blackman window
            double a0 = 0.42;
            double a1 = 0.5;
            double a2 = 0.08;
            return a0-(a1*cos((2*M_PI*n)/(N-1)))+(a2*cos((4*M_PI*n)/(N-1)));
        }break;

        case BARLET_HANNING:
        {//Barlett-Hann window
            double a0 = 0.62;
            double a1 = 0.48;
            double a2 = 0.38;
            return a0-(a1*abs((n/(N-1))-(1/2)))-(a2*cos((2*M_PI*n)/(N-1)));
        }break;

        default:
            return 1.0;
        break;
    }
    return 1.0;
}

double aPL::getNoiseSample(float A, int depression )
{
    switch(depression)
    {
        case UNIPOLAR:
        {
            return (double)(A*((double) rand() / (RAND_MAX)));
        }break;
        case BIPOLAR:
        {
            return (double)((double) rand() / (RAND_MAX))*(((double) rand() / (RAND_MAX))>=0.5)?(-A):(A);
        }break;
        default:
        {
            return (double)(A*((double) rand() / (RAND_MAX)));
        }
    }
}

double aPL::getAproxSquareSample(int fs, float A, float f, float fi, int n, int N, int M )
{
    UNUSED(N);
    double tmpSum = 0;
    for(int k=1; k<((2*M)+1); k++)
    {
        if((k%2)!=0)
        {
            tmpSum += sin(k*(2*M_PI*f*((float)((1.0f/fs)*n))+fi))/k;
        }/*else{
            tmpSum += 0;
        }*/
    }
    return (double)(((4*A)/M_PI)*tmpSum);
}

double aPL::getAproxSawSample( float fs, float A, float f, float fi, int n, int N, int M)
{
    UNUSED(N);
    double tmpSum = 0;
    for(int k=1; k<(M+1); k++)
    {
        if((k%2)!=0)
        {
            tmpSum += sin(k*(2*M_PI*f*((float)((1.0f/fs)*n))+fi))/k;
        }else{
            tmpSum -= sin(k*(2*M_PI*f*((float)((1.0f/fs)*n))+fi))/k;
        }
    }
    return (double)(((2*A)/M_PI)*tmpSum);
}

double aPL::getAproxTriangleSample( float fs, float A, float f, float fi, int n, int N, int M )
{
    UNUSED(N);
    double tmpSum = 0;
    bool q = true;
    for(int k=1; k<((2*M)+1); k++)
    {
        if((k%2)!=0)
        {
            if(q)
            {
                tmpSum += sin(k*(2*M_PI*f*((float)((1.0f/fs)*n))+fi))/(k*k);
            }else{
                tmpSum -= sin(k*(2*M_PI*f*((float)((1.0f/fs)*n))+fi))/(k*k);
            }
            q=!q;
        }
    }
    return (double)(((8*A)/(M_PI*M_PI))*tmpSum);
}

double aPL::getPerfectSineSample( float fs, float A, float f, float fi, int n, int N )
{
    //UNUSED(N);
    return A*sin(2*M_PI*f*((float)((1.0f/fs)*n))+fi);
}

double aPL::getPerfectSincSample( float fs, float A, float f, float fi, int n, int N )
{
    float T = (float)((1.0f/fs)*N);
    float t = (float)((1.0f/fs)*n);
    return (double)((A * sin( (2.0*M_PI*f*((-T/2)+t)) + fi ))/(2.0*M_PI*f*((-T/2)+t)) + fi );
}

double aPL::getPerfectSquareSample( float fs, float A, float f, float fi, int n, int N, int depression )
{
    UNUSED(N);
    switch(depression)
    {
        case UNIPOLAR:
        {
            return ((sin(2*M_PI*f*((float)((1.0f/fs)*n))+fi)>=0)?(A):(0));
        }break;
        case BIPOLAR:
        {
            return A*((sin(2*M_PI*f*((float)((1.0f/fs)*n))+fi)>=0)?(A):(-A));
        }break;
        default:
        {
            return A*((sin(2*M_PI*f*((float)((1.0f/fs)*n))+fi)>=0)?(A):(0));
        }break;
    }
    return 0.0;
}

double aPL::getPerfectSawSample( float fs, float A, float f, float fi, int n, int N )
{
    UNUSED(N);
    return fmod(A+((M_PI/8)*f*((float)((1.0f/fs)*n))+fi),2*A)-A;
}

double aPL::getPerfectTriangleSample( float fs, float A, float f, float fi, int n, int N )
{
    UNUSED(N);
    if(sin(2*M_PI*f*((float)((1.0f/fs)*n))+(M_PI/2)+fi)>0)
    {
        return fmod(A+(4*f*((float)((1.0f/fs)*n))*A),2*A)-A;
    }else{
        return fmod(A-(4*f*((float)((1.0f/fs)*n))*A),2*A)+A;
    }
    return 0.0;
}

void aPL::fft(int N, vector< complex <double> >* in, vector< complex<double> >* out)
{
    fftw_plan p;
    fftw_complex *_in, *_out;

    _in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    _out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

    for(int n=0;n<N;n++)
    {
        memcpy( _in[n], &(in->at(n)), sizeof( fftw_complex ) );
    }

    p = fftw_plan_dft_1d(N,_in,_out,FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(p);

    out->clear();

    complex<double> tmp;
    for(int n=0;n<N;n++)
    {
        tmp.real(_out[n][0]);
        tmp.imag(_out[n][1]);
        out->push_back(tmp);
    }

    fftw_destroy_plan(p);
    fftw_free(_in); fftw_free(_out);
}

void aPL::ifft(int N, vector< complex <double> >* in, vector< complex<double> >* out)
{
    fftw_plan p;
    fftw_complex *_in, *_out;

    _in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    _out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

    for(int n=0;n<N;n++)
    {
        memcpy( _in[n], &(in->at(n)), sizeof( fftw_complex ) );
    }

    p = fftw_plan_dft_1d(N,_in,_out,FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(p);

    out->clear();

    complex<double> tmp;
    for(int n=0;n<N;n++)
    {
        tmp.real(_out[n][0]/N);
        tmp.imag(_out[n][1]/N);
        out->push_back(tmp);
    }

    fftw_destroy_plan(p);
    fftw_free(_in); fftw_free(_out);
}

vector< complex<double> >* aPL::rxy(vector< complex<double> >* x, vector< complex<double> >* y)
{
    vector< complex<double> >* x_tmp = new vector< complex<double> >;
    vector< complex<double> >* X = new vector< complex<double> >;
    vector< complex<double> >* y_tmp = new vector< complex<double> >;
    vector< complex<double> >* Y = new vector< complex<double> >;

    int Nx = x->size();
    int Ny = y->size();
    int N = (Nx>Ny)?(Nx<<1):(Ny<<1);

    for(int n=0;n<N;n++)
    {
        x_tmp->push_back((n<Nx)?(x->at(n).real()):(0));
        y_tmp->push_back((n<Ny)?(y->at(n).real()):(0));
    }

    fft(N, x_tmp, X);
    fft(N, y_tmp, Y);

    for(int n=0;n<N;n++)
    {
        Y->at(n).imag(Y->at(n).imag()*(-1));
        X->at(n)=X->at(n)*Y->at(n);
    }

    Y->clear();

    ifft(N,X,Y);

    delete(x_tmp);
    delete(y_tmp);

    X->clear();
    int k=floor(N/2);
    for(int n=0;n<N;n++)
    {
        X->push_back(Y->at(k));
        k++;
        if(k>=N)
            k=0;
    }

    delete(Y);

    return X;

    //X = fft(x,2^13);%for 4410 samples and radix-2 should be N=8192 point fft
    //Y = fft(y,2^13);
    //   c = ifft(X.*conj(Y));
    //   c = [c(end-M+2:end,:);c(1:M,:)];
}

vector<complex<double> > *aPL::add(vector< complex<double> >* a, vector< complex<double> >* b)
{
    int Na = a->size();
    int Nb = b->size();
    int N = (Na>Nb)?(Na):(Nb);

    vector< complex<double> >* c = new vector< complex<double> >[N];

    complex<double> tmp;
    for(int n=0;n<N;n++)
    {
        tmp.real(((n<Na)?(a->at(n).real()):(0)) + ((n<Nb)?(b->at(n).real()):(0)));
        tmp.imag(0);
        c->push_back(tmp);
    }

    return c;
}

vector<complex<double> > *aPL::sub(vector< complex<double> >* a, vector< complex<double> >* b)
{
    int Na = a->size();
    int Nb = b->size();
    int N = (Na>Nb)?(Na):(Nb);

    vector< complex<double> >* c = new vector< complex<double> >[N];

    complex<double> tmp;
    for(int n=0;n<N;n++)
    {
        tmp.real(((n<Na)?(a->at(n).real()):(0)) - ((n<Nb)?(b->at(n).real()):(0)));
        tmp.imag(0);
        c->push_back(tmp);
    }

    return c;
}

vector<complex<double> > *aPL::mul(vector< complex<double> >* a, vector< complex<double> >* b)
{
    int Na = a->size();
    int Nb = b->size();
    int N = (Na>Nb)?(Na):(Nb);

    vector< complex<double> >* c = new vector< complex<double> >[N];

    complex<double> tmp;
    for(int n=0;n<N;n++)
    {
        tmp.real(((n<Na)?(a->at(n).real()):(1)) * ((n<Nb)?(b->at(n).real()):(1)));
        tmp.imag(0);
        c->push_back(tmp);
    }

    return c;
}

vector<complex<double> > *aPL::div(vector< complex<double> >* a, vector< complex<double> >* b)
{
    int Na = a->size();
    int Nb = b->size();
    int N = (Na>Nb)?(Na):(Nb);

    vector< complex<double> >* c = new vector< complex<double> >[N];

    complex<double> tmp;
    for(int n=0;n<N;n++)
    {
        tmp.real(((n<Na)?(a->at(n).real()):(0)) / ((n<Nb)?(b->at(n).real()):(1)));
        tmp.imag(0);
        c->push_back(tmp);
    }

    return c;
}

vector< complex<double> >*  aPL::windowing(vector< complex<double> >* x, int typeWindow)
{
    int N = x->size();

    vector< complex<double> >* c = new vector< complex<double> >[N];

    complex<double> tmp;
    for(int n=0;n<N;n++)
    {
        tmp.real(x->at(n).real() * windowFunction(typeWindow,n,N));
        tmp.imag(0);
        c->push_back(tmp);
    }

    return c;
}

int aPL::RtMidiInTest()
{
    int error_code = 0;
    RtMidiIn  *midiin = 0;

    try {
        // Default RtMidiIn constructor
        midiin = new RtMidiIn();

        // Check inputs
        if(midiin->getPortCount() == 0){
            error_code = 1;
        }
    } catch ( RtMidiError &error ) {
        error.printMessage();
        error_code = 1;
    }

    if(!midiin){
        error_code = 1;
    }

    delete midiin;
    return error_code;
}

bool aPL::RtMidiInInit()
{
    try {
        this->rtMidiIn = new RtMidiIn();
        this->inMidiPotrCount = this->rtMidiIn->getPortCount();
    } catch ( RtMidiError &error ) {
        error.printMessage();
        return false;
    }
    return true;
}

int aPL::RtMidiOutTest()
{
    int error_code = 0;
    RtMidiOut *midiout = 0;

    try {
        // Default RtMidiOut constructor
        midiout = new RtMidiOut();

        // Check outputs
        if(midiout->getPortCount() == 0){
            error_code =1;
        }

    } catch ( RtMidiError &error ) {
        error.printMessage();
        error_code = 1;
    }

    if(!midiout){
        error_code = 1;
    }

    delete midiout;
    return error_code;
}

int aPL::RtAudioTest()
{
    int error_code = 0;
    RtAudio *audio = 0;

    try {
        // Default RtAudio constructor
        audio = new RtAudio();
    }
    catch (RtAudioError &error) {
        error.printMessage();
        error_code = 1;
    }

    if(!audio){
        error_code = 1;
    }

    // Clean up
    delete audio;

    return error_code;
}

int aPL::RtAudioCallbackFcn(
        void			*outbuf,
        void			*inbuf,
        unsigned int		nFrames,
        double			streamtime,
        RtAudioStreamStatus	status,
        void			*userdata)
{
    UNUSED(streamtime);
    UNUSED(status);
    (void)inbuf;
    float	*buf = (float*)outbuf;
    unsigned int remainFrames;
    RtAudioCallbackDataStruct	*rtAudioCallbackData = (RtAudioCallbackDataStruct*)userdata;

    remainFrames = nFrames;
    while (remainFrames > 0) {
        unsigned int sz = rtAudioCallbackData->nFrame - rtAudioCallbackData->cur;
        if (sz > remainFrames)
            sz = remainFrames;
        memcpy(buf, rtAudioCallbackData->wftable+(rtAudioCallbackData->cur*rtAudioCallbackData->nChannel),
               sz * rtAudioCallbackData->nChannel * sizeof(float));
        rtAudioCallbackData->cur = (rtAudioCallbackData->cur + sz) % rtAudioCallbackData->nFrame;
        buf += sz * rtAudioCallbackData->nChannel;
        remainFrames -= sz;
    }
    return 0;
}

bool aPL::RtAudioInit()
{
    try {
        rtAudio = new RtAudio();
    }catch  (RtAudioError e){
        return false;
    }
    if (!rtAudio){
        return false;
    }

    rtAudioDevId = rtAudio->getDefaultOutputDevice(); //probe rtAudio devices
    rtAudioOutParam = new RtAudio::StreamParameters(); //Setup output stream parameters
    rtAudioOutParam->deviceId = rtAudioDevId;
    rtAudioOutParam->nChannels = 2;
    rtAudioBufsize = 512;
    rtAudio->openStream(
                rtAudioOutParam,
                NULL,
                RTAUDIO_FLOAT32,
                44100,
                &rtAudioBufsize,
                RtAudioCallbackFcn,
                &rtAudioCallbackData
                );

    rtAudioCallbackData.nRate = 44100;
    rtAudioCallbackData.nFrame = 44100;
    rtAudioCallbackData.nChannel = rtAudioOutParam->nChannels;
    rtAudioCallbackData.cur = 0;
    rtAudioCallbackData.wftable = (float *)calloc(rtAudioCallbackData.nChannel * rtAudioCallbackData.nFrame, sizeof(float));

    if (!rtAudioCallbackData.wftable)
    {
        delete rtAudio;
        return false;
    }

    for(unsigned int i = 0; i < rtAudioCallbackData.nFrame; i++)
    {
        //for(unsigned int j = 0; j < rtAudioCallbackData.nChannel; j++)
        //{
          //  rtAudioCallbackData.wftable[i*rtAudioCallbackData.nChannel+j] = 0.0f;
        //}
        rtAudioCallbackData.wftable[i*rtAudioCallbackData.nChannel+0] = 0.0f;
        rtAudioCallbackData.wftable[i*rtAudioCallbackData.nChannel+1] = 0.0f;
    }
    //cout<<"init:"<<rtAudioCallbackData.wftable[0]<<endl<<
    //    rtAudioCallbackData.wftable[1]<<endl;
    rtAudio->startStream();
    return true;
}

float *aPL::RtAudioGetWaveForm(){
    return rtAudioCallbackData.wftable;
}


void aPL::TRIG_KEY(float f)
{
    last_f_key = f;

    if(rtAudio)
    {
        //rtAudio->stopStream();
    }

    float A = 1.0f;

    for(unsigned int i = 0; i < rtAudioCallbackData.nFrame; i++)
    {
        double v=0.0;

        double lfo1 = LFO(0, i, rtAudioCallbackData.nFrame);
        double lfo2 = LFO(1, i, rtAudioCallbackData.nFrame);

        float f1= (f + (osc[0].fine)) + (osc[0].coarse);
        float f2= (f + (osc[1].fine)) + (osc[1].coarse);

        float osc1_v=0.0f;
        float osc2_v=0.0f;

        switch(osc[0].shape){
            case NOISE: osc1_v = getNoiseSample( A, BIPOLAR );break;
            case SIN: osc1_v = getPerfectSineSample( 44100.0f,  A, (float)(f1+lfo1), 0.0f, i, 44100 );break;
            case SAW: osc1_v= getPerfectSawSample( 44100,  A, (float)(f1+lfo1), 0, i, 44100 );break;
            case SQUARE: osc1_v= getPerfectSquareSample( 44100,  A, (float)(f1+lfo1), 0, i, 44100, BIPOLAR );break;
            default:osc1_v=1.0f;
        }

        switch(osc[1].shape){
            case NOISE: osc2_v = getNoiseSample( A, BIPOLAR );break;
            case SIN: osc2_v = getPerfectSineSample( 44100,  A, (float)(f2+lfo2), 0, i, 44100 );break;
            case SAW: osc2_v= getPerfectSawSample( 44100,  A, (float)(f2+lfo2), 0, i, 44100 );break;
            case SQUARE: osc2_v= getPerfectSquareSample( 44100, A, (float)(f2+lfo2), 0, i, 44100, BIPOLAR );break;
            default:osc2_v=1.0f;
        }

        v=(double)((osc1_v*osc[0].mix)+(osc2_v*osc[1].mix));

        int offsetL = i*rtAudioCallbackData.nChannel+0;
        int offsetR = i*rtAudioCallbackData.nChannel+1;
        rtAudioCallbackData.wftable[offsetL] = v;
        rtAudioCallbackData.wftable[offsetR] = v;
    }

    if(rtAudio)
    {
        //rtAudio->startStream();
    }

}


void aPL::updateBuffer()
{
    float f = last_f_key;

    if(rtAudio)
    {
        //rtAudio->stopStream();
    }

    float A = 1.0f;

    for(unsigned int i = 0; i < rtAudioCallbackData.nFrame; i++)
    {
        double v=0.0;

        double lfo1 = LFO(0, i, rtAudioCallbackData.nFrame);
        double lfo2 = LFO(1, i, rtAudioCallbackData.nFrame);

        float f1= (f + (osc[0].fine)) + (osc[0].coarse);
        float f2= (f + (osc[1].fine)) + (osc[1].coarse);

        float osc1_v=0.0f;
        float osc2_v=0.0f;

        switch(osc[0].shape){
            case NOISE: osc1_v = getNoiseSample( A, BIPOLAR );break;
            case SIN: osc1_v = getPerfectSineSample( 44100.0f,  A, (float)(f1+lfo1), 0.0f, i, 44100 );break;
            case SAW: osc1_v= getPerfectSawSample( 44100,  A, (float)(f1+lfo1), 0, i, 44100 );break;
            case SQUARE: osc1_v= getPerfectSquareSample( 44100,  A, (float)(f1+lfo1), 0, i, 44100, BIPOLAR );break;
            default:osc1_v=1.0f;
        }

        switch(osc[1].shape){
            case NOISE: osc2_v = getNoiseSample( A, BIPOLAR );break;
            case SIN: osc2_v = getPerfectSineSample( 44100,  A, (float)(f2+lfo2), 0, i, 44100 );break;
            case SAW: osc2_v= getPerfectSawSample( 44100,  A, (float)(f2+lfo2), 0, i, 44100 );break;
            case SQUARE: osc2_v= getPerfectSquareSample( 44100, A, (float)(f2+lfo2), 0, i, 44100, BIPOLAR );break;
            default:osc2_v=1.0f;
        }

        v=(double)((osc1_v*osc[0].mix)+(osc2_v*osc[1].mix));

        int offsetL = i*rtAudioCallbackData.nChannel+0;
        int offsetR = i*rtAudioCallbackData.nChannel+1;
        rtAudioCallbackData.wftable[offsetL] = v;
        rtAudioCallbackData.wftable[offsetR] = v;
    }

    if(rtAudio)
    {
        //rtAudio->startStream();
    }

}
