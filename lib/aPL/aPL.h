/**
 * Description: Audio processing library
 * Author: WiWav
 * File: aPL.h
 */

#include <conio.h>
#include <iostream>
#include <stdlib.h>
//#include <thread>
#include <utility>
#include <math.h>
#include <complex>

#include <fftw3.h>
#include <sndfile.h>

#include <RtMidi.h>
#include <RtAudio.h>


using namespace std;

#ifndef APL
#define APL

/**
 * Definition for userfrendly configuration input devices
 */

#define HEXFILE     0
#define WAVFILE     1
#define KEYBOARD    2
#define MIDI        3
#define BRAIN       4
#define TEST_RUN    5


/**
 * Definition for userfrendly configuration shape signal
 * (for "int shape")
 */
#define UNKNOWN_SHAPE -3
#define IFFT_SHAPE    -2
#define FFT_SHAPE     -1
#define SIN            0
#define SAW            1
#define SAW_APROX      2
#define TRIANGLE       3
#define TRIANGLE_APROX 4
#define SQUARE         5
#define SQUARE_APROX   6
#define NOISE          7
#define SINC           8


/**
 * Definition for userfrendly configuration unipolar and bipolar depression
 */
#define UNKNOWNPOLAR -1
#define UNIPOLAR      0
#define BIPOLAR       1

/**
 * Definition for arithmetic operations
 */
#define ADD 0
#define SUB 1
#define MUL 2
#define DIV 3

/**
 * Definition for windowing type function
 */
#define HAMMING         0  //Hamming window
#define HANNING         1  //Hanning(Hann) window
#define BLACKMAN_HARRIS 2  //Blackman-Harris window
#define BLACKMAN        3  //Blackman window
#define BARLET_HANNING  4  //Barlett-Hann window


#define UNUSED(expr) do { (void)(expr); } while (0)

class aPL
{

public:
    /**
     * CONSTRUCTOR AND DESTRUCTOR
     */

    aPL();
    ~aPL();

    /**
     * FUNCTIONS AND VARIABLES FOR LIBRARY RtMidi
     */
    RtMidiIn *rtMidiIn;
    unsigned int inMidiPotrCount;
    int RtMidiInTest();
    bool RtMidiInInit();
    bool RtMidiInClose();

    RtMidiOut rtMidiOut;
    int RtMidiOutTest();
    bool RtMidiOutInit();

    /**
    * FUNCTIONS AND VARIABLES FOR LIBRARY RtAudio.
    */
    RtAudio *rtAudio;
    int RtAudioTest();

    unsigned int rtAudioDevId;// = rtAudio->getDefaultOutputDevice(); //probe rtAudio devices
    RtAudio::StreamParameters *rtAudioOutParam;// = new RtAudio::StreamParameters(); //Setup output stream parameters
    unsigned int rtAudioBufsize;

    typedef struct {
        unsigned int	nRate;		/* Sampling Rate (sample/sec) */
        unsigned int	nChannel;	/* Channel Number */
        unsigned int	nFrame;		/* Frame Number of Wave Table */
        float		*wftable;	/* Wave Form Table(interleaved) */
        unsigned int	cur;		/* current index of WaveFormTable(in Frame) */
    } RtAudioCallbackDataStruct;

    RtAudioCallbackDataStruct rtAudioCallbackData;

    static int RtAudioCallbackFcn(
            void			*outbuf,
            void			*inbuf,
            unsigned int		nFrames,
            double			streamtime,
            RtAudioStreamStatus	status,
            void			*userdata);

    bool RtAudioInit();

    float *RtAudioGetWaveForm();


    /**
     * SYNTHESIS METHODS
     */

    double fs;
    float *buffer;
    float last_f_key;

    void loop();//probably to delete
    void execute();//probably to delete

    aPL* in(aPL* unit);
    aPL* in(aPL* unit1, aPL* unit2);
    aPL* in(aPL** units);
    aPL* in(int clock);
    aPL* in(int clock, int device);///string
    void in(double inValue);
    void in(void* inArrayOrVectorValue);

    aPL* out(/*out params*/);
    double outV();//one sample value in clock tact /// na float  przerobic
    void* outA();//out Array Or Vector Value


    //LFO's var
    struct lfoParams
    {
        float amount;
        int rate;
        int shape;
    }lfo[2];

    aPL* LFO(int numberLFO, float amount, float rate, int shape);
    double LFO(int numberLFO, int n, int N);

    aPL* EG();

    aPL* VCO();

    //OSC
    struct oscParams
    {
        float coarse;
        float fine;
        int shape;
        float mix;
    }osc[2];
    aPL* OSC(int numberOSC, float coarse, float fine, int shape, int mix);

    aPL* DCO();

    /*VCF's*/
    aPL* LPF();
    aPL* HPF();
    aPL* BPF();

    aPL* VCA();

    aPL* FX();

    aPL* MUX();

    void TRIG_KEY(float f);
    void updateBuffer();


    /**
     * METHODS AND ALGORITHMS PROCESSING ACOUSTIC AND DIGITAL SIGNALS
     */

    //generators
    double getNoiseSample(float A, int depression);
    double getAproxSquareSample(int fs, float A, float f, float fi, int n, int N , int M);
    double getAproxSawSample(float fs, float A, float f, float fi, int n, int N , int M);
    double getAproxTriangleSample(float fs, float A, float f, float fi, int n, int N , int M);
    double getPerfectSineSample( float fs, float A, float f, float fi, int n, int N );
    double getPerfectSincSample( float fs, float A, float f, float fi, int n, int N );
    double getPerfectSquareSample(float fs, float A, float f, float fi, int n, int N , int depression);
    double getPerfectSawSample( float fs, float A, float f, float fi, int n, int N );
    double getPerfectTriangleSample( float fs, float A, float f, float fi, int n, int N );

    //spectrum
    void fft(int N, vector< complex<double> > *in, vector< complex<double> >* out);
    void ifft(int N, vector< complex<double> > *in, vector< complex<double> >* out);

    //correlation
    vector< complex<double> >* rxy(vector< complex<double> >* x, vector< complex<double> >* y);

    //aritmethic operations
    vector< complex<double> >*  add(vector< complex<double> >* a, vector< complex<double> >* b);
    vector< complex<double> >*  sub(vector< complex<double> >* a, vector< complex<double> >* b);
    vector< complex<double> >*  mul(vector< complex<double> >* a, vector< complex<double> >* b);
    vector< complex<double> >*  div(vector< complex<double> >* a, vector< complex<double> >* b);

    //windowing
    double windowFunction(int typeWindow, int n, int N);
    vector< complex<double> >*  windowing(vector< complex<double> >* x, int typeWindow);

};

#endif // APL
