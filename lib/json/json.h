#ifndef JSON_H
#define JSON_H

#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QFile>
#include <QString>

class json
{
public:
    json(){}
    ~json(){}
    virtual void read(const QJsonObject &jsonObj) = 0;
    virtual void write(QJsonObject &jsonObj) const = 0;
    bool load(QString jsonFilePath);
    bool save(QString jsonFilePath);
};

#endif // JSON_H
