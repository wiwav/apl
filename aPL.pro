#-------------------------------------------------
#
# Project created by QtCreator 2016-05-16T16:39:34
#
#-------------------------------------------------

QT += core gui multimedia
#DEBUG = -g
#LIBS += -lm -lstdc++
LIBS += -lole32 -lwinmm -luuid -lksuser # standard lib's
LIBS += -Llib\fftw -lfftw3 -lpsapi      # fftw3 -lfftw3
LIBS += -Llib\libsndfile -lsndfile-1    # libsndfile

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

DEFINES += __WINDOWS_WASAPI__ # define for RtAudio
DEFINES += __WINDOWS_MM__     # define for RtMidi

TARGET = aPL
TEMPLATE = app

INCLUDEPATH += \
        src/ \
        lib/ \
        lib/aPL/ \
        lib/RtMidi/ \
        lib/RtAudio/ \
        lib/RtAudio/inc/ \
        lib/QCustomPlot/ \
        lib/json/ \
        lib/fftw/ \
        lib/libsndfile/ \

SOURCES += src/main.cpp \
        src/worker.cpp \
        src/settings.cpp \
        src/waveform.cpp \
        ui/GUIaPL.cpp \
        lib/aPL/aPL.cpp \
        lib/RtMidi/RtMidi.cpp \
        lib/RtAudio/RtAudio.cpp \
        lib/QCustomPlot/QCustomPlot.cpp \
        lib/json/json.cpp \


HEADERS  += src/worker.h \
        src/settings.h \
        src/waveform.h \
        ui/GUIaPL.h \
        lib/aPL/aPL.h \
        lib/RtMidi/RtMidi.h \
        lib/RtAudio/RtAudio.h \
        lib/QCustomPlot/QCustomPlot.h \
        lib/json/json.h \
        lib/fftw/fftw3.h \
        lib/libsndfile/sndfile.h \


FORMS    += ui/GUIaPL.ui
UI_DIR = ui

RESOURCES = res/res.qrc

DESTDIR     = builds
OBJECTS_DIR = builds/objects
MOC_DIR     = builds/mocs
