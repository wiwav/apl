#include <waveform.h>
#include <aPL.h>
#include <math.h>

waveform::waveform(int _shape, int _M, int _depression, aPL *_engineHandle)
{
    engine = _engineHandle;
    shape = _shape;
    M = _M;
    depression = _depression;
    collection = new vector < complex < double > >;
}

waveform::waveform()
{}

void waveform::fill(float _fs, float _A, float _f, float _fi, float _t)
{
    fs = _fs;
    A = _A;
    f = _f;
    fi = _fi;
    t = _t;
    N = ceil(t * (double)(fs));

    switch(shape)
    {
        case SINC:
        {
            for(int n=0; n<N; n++)
            {
                collection->push_back(engine->getPerfectSincSample(fs,A,f,fi,n,N));
                timeField.push_back(n/fs);
            }
        }break;
        case SIN:
        {
            for(int n=0; n<N; n++)
            {
                collection->push_back(engine->getPerfectSineSample(fs,A,f,fi,n,N));
                timeField.push_back(n/fs);
            }
        }break;

        case SAW:
        {
            for(int n=0; n<N; n++)
            {
                collection->push_back(engine->getPerfectSawSample(fs,A,f,fi,n,N));
                timeField.push_back(n/fs);
            }
        }break;
        case SAW_APROX:
        {
            for(int n=0; n<N; n++)
            {
                collection->push_back(engine->getAproxSawSample(fs,A,f,fi,n,N,M));
                timeField.push_back(n/fs);
            }
        }break;
        case TRIANGLE:
        {
            for(int n=0; n<N; n++)
            {
                collection->push_back(engine->getPerfectTriangleSample(fs,A,f,fi,n,N));
                timeField.push_back(n/fs);
            }
        }break;
        case TRIANGLE_APROX:
        {
            for(int n=0; n<N; n++)
            {
                collection->push_back(engine->getAproxTriangleSample(fs,A,f,fi,n,N,M));
                timeField.push_back(n/fs);
            }
        }break;
        case SQUARE:
        {
            for(int n=0; n<N; n++)
            {
                collection->push_back(engine->getPerfectSquareSample(fs,A,f,fi,n,N,depression));
                timeField.push_back(n/fs);
            }
        }break;
        case SQUARE_APROX:
        {
            for(int n=0; n<N; n++)
            {
                collection->push_back(engine->getAproxSquareSample(fs,A,f,fi,n,N,M));
                timeField.push_back(n/fs);
            }
        }break;
        case NOISE:
        {
            for(int n=0; n<N; n++)
            {
                collection->push_back(engine->getNoiseSample(A,depression));
            }
        }break;
    }
}

void waveform::doDFT()
{


}
