#include "worker.h"

Worker::Worker(QObject *parent) : QObject(parent)
{
}

void Worker::setWorker(QCustomPlot *handler, float *data)
{
    handlerObj = handler;
    dataVector = data;
}

void Worker::hardWork()//onTimeout
{
    qDebug()<<"Worker::onTimeout get called from?: "<<QThread::currentThreadId();
    QVector <double> L(44101), R(44101),x(44101);

    for(int i = 0; i < 44101 ; i++)
    {
       x[i] = i;
       L[i] = dataVector[(i-1)*2+0]+1;
       R[i] = dataVector[(i-1)*2+1]-1;
    }
    //handlerObj->addGraph();
    handlerObj->graph(0)->setData(x,L);

    //handlerObj->addGraph();
    handlerObj->graph(1)->setData(x,R);
    //handlerObj->xAxis->setRange(0, 44100);
    //handlerObj->yAxis->setRange(-2, 2);
    handlerObj->replot();
}
