#include "settings.h"

#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDir>
#include <QMessageBox>

settings::settings()
{
    this->changedSettingsStatus = false;
}

settings::~settings()
{
    if(this->changedSettingsStatus==true)
    {
        QMessageBox msgBox;
        msgBox.setText("Settings has been modified.");
        msgBox.setInformativeText("Do you want to save your changes?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard);
        msgBox.setDefaultButton(QMessageBox::Save);
        switch (msgBox.exec())
        {
            case QMessageBox::Save:
                this->save(this->basePath+"\\"+this->settingsFilePath);
            break;
            case QMessageBox::Discard:
                // Don't Save was clicked
            break;
            default:
            break;
        }
    }
}

bool settings::init(QString settingsFilePath)
{
    this->settingsFilePath = settingsFilePath;
    QDir dir;
    this->basePath = dir.absolutePath().replace("/","\\");
    this->load(this->basePath+"\\"+this->settingsFilePath);
    return true;
}

void settings::markChanges()
{
    this->changedSettingsStatus = true;
}

void settings::unmarkChanges()
{
    this->changedSettingsStatus = false;
}

void settings::saveChanges()
{
    this->save(this->basePath+"\\"+this->settingsFilePath);
    this->changedSettingsStatus = false;
}

void settings::read(const QJsonObject &json)
{
    this->jsonObj = json;
}

void settings::write(QJsonObject &json) const
{
    json = this->jsonObj;
}

int settings::getInt(QString name)
{
    return this->jsonObj[name].toInt();
}

void settings::setInt(QString name, int value)
{
    this->markChanges();
    this->jsonObj[name] = value;
}

bool settings::getBool(QString name)
{
    return this->jsonObj[name].toBool();
}

void settings::setBool(QString name, bool value)
{
    this->markChanges();
    this->jsonObj[name] = value;
}

double settings::getDouble(QString name)
{
    return this->jsonObj[name].toDouble();
}

void settings::setDouble(QString name, double value)
{
    this->markChanges();
    this->jsonObj[name] = value;
}

QString settings::getString(QString name)
{
    return this->jsonObj[name].toString();
}

void settings::setString(QString name, QString value)
{
    this->markChanges();
    this->jsonObj[name] = value;
}
