#ifndef WAVEFORM_H
#define WAVEFORM_H
#include <aPL.h>
#include <vector>
#include <complex>

class waveform //: private aPL
{
public:

    waveform();
    waveform(int _shape, int _M, int _depression, aPL *_engineHandle);
    void fill(float _fs, float _A, float _f, float _fi, float _t);
    //~waveform();

public:
    aPL *engine;
    int shape;
    int M;
    int depression;

    float fs;
    float A;
    float f;
    float fi;
    float t;
    int N;

    //vector < double > collection;
    vector < complex < double > >* collection;
    vector < double > timeField;
    complex < double > coll;

    void doDFT();
    //void doIDFT();
    complex < double > dft_collection;
    complex < double > idft_collection;

    waveform operator + ( waveform &tmp );
    waveform operator + ( const long double q );
    waveform operator - ( waveform &tmp );
    waveform operator - ( const long double q );
    waveform operator * ( waveform &tmp );
    waveform operator * ( const long double q );
    waveform operator / ( waveform &tmp );
    waveform operator / ( const long double q );
};

#endif // WAVEFORM_H
