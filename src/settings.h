#ifndef SETTINGS_H
#define SETTINGS_H

#include <json.h>
#include <QString>

class settings : private json
{
public:
    settings();
    ~settings();

    bool init(QString settingsFilePath);
    void markChanges();
    void unmarkChanges();
    void saveChanges();

private:
    //settings configuration
    bool changedSettingsStatus;
    QString settingsFilePath;
    QString basePath;

    //wrapper for jsonBase
    QJsonObject jsonObj;
    void read(const QJsonObject &jsonObj);
    void write(QJsonObject &jsonObj) const;

public:
    //getters and setters
    int getInt(QString name);
    void setInt(QString name, int value);

    bool getBool(QString name);
    void setBool(QString name, bool value);

    double getDouble(QString name);
    void setDouble(QString name, double value);

    QString getString(QString name);
    void setString(QString name, QString value);

};

#endif // SETTINGS_H
