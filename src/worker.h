#ifndef WORKER_H
#define WORKER_H

#include <QtCore>
#include <qdial.h>
#include "QCustomPlot.h"

class Worker : public QObject
{
    Q_OBJECT
public:
    explicit Worker(QObject *parent = 0);
    void setWorker(QCustomPlot *handler , float *data);

private slots:
    void hardWork();//onTimeout

private:
    QCustomPlot *handlerObj;
    float *dataVector;
};

#endif // WORKER_H
