Require:
builds/fftw3.dll
builds/WIwav.png
builds/conf.json

e.g
conf.json::
{
    "ampEnvelopeAtkDial": 250,
    "ampEnvelopeDecDial": 200,
    "ampEnvelopeRelDial": 200,
    "ampEnvelopeSusDial": 95,
    "filCutOffDial": 9,
    "filEnvelopeAtkDial": 154,
    "filEnvelopeDecDial": 200,
    "filEnvelopeRelDial": 200,
    "filEnvelopeSusDial": 38,
    "filResoDial": 66,
    "lfo1AmountDial": 100,
    "lfo1RateDial": 3164,
    "lfo1RateOffDial": null,
    "lfo2AmountDial": 100,
    "lfo2RateDial": 20000,
    "lfo_1Shape": 1,
    "lfo_2Shape": 1,
    "osc1CoarseDial": 109,
    "osc1FineDial": 0,
    "osc1_Shape": 1,
    "osc1_mix": 71,
    "osc2CoarseDial": 115,
    "osc2FineDial": 0,
    "osc2_Shape": 0,
    "osc2_mix": 29,
    "test": 171
}
