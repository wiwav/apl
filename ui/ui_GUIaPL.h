/********************************************************************************
** Form generated from reading UI file 'GUIaPL.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GUIAPL_H
#define UI_GUIAPL_H

#include <QCustomPlot.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDial>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QStackedWidget *stackedWidget;
    QWidget *mainPage;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *mainLayout;
    QHBoxLayout *mainControlLayout;
    QLabel *mainTitleLabel;
    QSpacerItem *mainControlLayoutSpacer;
    QPushButton *mainBrainButton;
    QPushButton *mainSettingsButton;
    QPushButton *mainCloseButton;
    QHBoxLayout *mainContentLayout;
    QVBoxLayout *mainLFOLayout;
    QHBoxLayout *mainLFOSliderLayout;
    QVBoxLayout *onlyLFOLayout;
    QGroupBox *lfo_1GroupBox;
    QVBoxLayout *verticalLayout_19;
    QHBoxLayout *horizontalLayout_12;
    QVBoxLayout *verticalLayout_4;
    QDial *lfo1AmountDial;
    QLabel *label_13;
    QVBoxLayout *verticalLayout_18;
    QDial *lfo1RateDial;
    QLabel *label_14;
    QHBoxLayout *horizontalLayout_13;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *lfo_1ShapeSaw;
    QPushButton *lfo_1ShapeSquare;
    QPushButton *lfo_1ShapeSin;
    QPushButton *lfo_1ShapeNoise;
    QSpacerItem *horizontalSpacer_5;
    QGroupBox *lfo_2GroupBox;
    QVBoxLayout *verticalLayout_17;
    QHBoxLayout *horizontalLayout_10;
    QVBoxLayout *verticalLayout_15;
    QDial *lfo2AmountDial;
    QLabel *label_11;
    QVBoxLayout *verticalLayout_16;
    QDial *lfo2RateDial;
    QLabel *label_12;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *lfo_2ShapeSaw;
    QPushButton *lfo_2ShapeSquare;
    QPushButton *lfo_2ShapeSin;
    QPushButton *lfo_2ShapeNoise;
    QSpacerItem *horizontalSpacer_6;
    QVBoxLayout *verticalLayout_12;
    QGroupBox *lfo_1DestGroupBox;
    QVBoxLayout *verticalLayout_31;
    QRadioButton *radioButton_5;
    QRadioButton *radioButton_6;
    QGroupBox *lfoMixGroupBox;
    QVBoxLayout *verticalLayout_13;
    QCheckBox *checkBox;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QGroupBox *lfo_2DestGroupBox;
    QVBoxLayout *verticalLayout_32;
    QRadioButton *radioButton_8;
    QRadioButton *radioButton_10;
    QVBoxLayout *verticalLayout_14;
    QLabel *label_7;
    QSlider *lfoSlider;
    QGroupBox *filtherGroupBox;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_3;
    QDial *filResoDial;
    QLabel *label_2;
    QVBoxLayout *verticalLayout_2;
    QDial *filCutOffDial;
    QLabel *label;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_14;
    QPushButton *keyC;
    QPushButton *keyD;
    QPushButton *keyE;
    QPushButton *keyF;
    QPushButton *keyG;
    QPushButton *keyA;
    QPushButton *keyH;
    QFrame *line;
    QVBoxLayout *mainOSCLayout;
    QHBoxLayout *mainOSCSliderLayout;
    QVBoxLayout *verticalLayout_6;
    QGroupBox *osc_1GroupBox;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_8;
    QDial *osc1CoarseDial;
    QLabel *label_3;
    QDial *osc1FineDial;
    QLabel *label_4;
    QVBoxLayout *verticalLayout_7;
    QPushButton *osc1Saw;
    QPushButton *osc1Squa;
    QPushButton *osc1Sin;
    QPushButton *osc1Nois;
    QGroupBox *osc_2GroupBox;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_10;
    QDial *osc2CoarseDial;
    QLabel *label_5;
    QDial *osc2FineDial;
    QLabel *label_6;
    QVBoxLayout *verticalLayout_9;
    QPushButton *osc2Saw;
    QPushButton *osc2Squa;
    QPushButton *osc2Sin;
    QPushButton *osc2Nois;
    QVBoxLayout *verticalLayout_30;
    QLabel *label_9;
    QSlider *oscSlider;
    QFrame *line_2;
    QVBoxLayout *mainENVLayout;
    QGroupBox *ampEnvGroupBox;
    QVBoxLayout *verticalLayout_20;
    QCustomPlot *ampEnvPlot;
    QHBoxLayout *horizontalLayout_15;
    QVBoxLayout *verticalLayout_21;
    QDial *ampEnvelopeAtkDial;
    QLabel *label_15;
    QVBoxLayout *verticalLayout_22;
    QDial *ampEnvelopeDecDial;
    QLabel *label_16;
    QVBoxLayout *verticalLayout_23;
    QDial *ampEnvelopeSusDial;
    QLabel *label_17;
    QVBoxLayout *verticalLayout_24;
    QDial *ampEnvelopeRelDial;
    QLabel *label_18;
    QGroupBox *filEnvGroupbox;
    QVBoxLayout *verticalLayout_29;
    QCustomPlot *fillEnvPlot;
    QHBoxLayout *horizontalLayout_16;
    QVBoxLayout *verticalLayout_25;
    QDial *filEnvelopeAtkDial;
    QLabel *label_19;
    QVBoxLayout *verticalLayout_26;
    QDial *filEnvelopeDecDial;
    QLabel *label_20;
    QVBoxLayout *verticalLayout_27;
    QDial *filEnvelopeSusDial;
    QLabel *label_21;
    QVBoxLayout *verticalLayout_28;
    QDial *filEnvelopeRelDial;
    QLabel *label_22;
    QFrame *line_3;
    QVBoxLayout *mainMonitorLayout;
    QGroupBox *monitroGroupbox;
    QVBoxLayout *verticalLayout_11;
    QCustomPlot *monitorPlot;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_38;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *pushButton_4;
    QLabel *wiwav;
    QLabel *label_23;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *mainFooterLayout;
    QVBoxLayout *verticalLayout_53;
    QWidget *algorithmPage;
    QVBoxLayout *verticalLayout_33;
    QHBoxLayout *algorithmControlLayout;
    QLabel *settingsTitleLabel_2;
    QSpacerItem *settingsControlLayoutSpacer_2;
    QPushButton *algorithmBackButton;
    QPushButton *algorithmCloseButton;
    QTabWidget *tabWidget_4;
    QWidget *tab_17;
    QHBoxLayout *horizontalLayout_50;
    QGroupBox *groupBox_17;
    QVBoxLayout *verticalLayout_73;
    QLabel *label_43;
    QLineEdit *nameToAdd;
    QFrame *line_24;
    QLabel *label_44;
    QSpinBox *sampFreqSpinBox;
    QFrame *line_26;
    QLabel *label_45;
    QDoubleSpinBox *ampSpinBox;
    QFrame *line_28;
    QLabel *label_46;
    QDoubleSpinBox *freqSpinBox;
    QFrame *line_30;
    QLabel *label_8;
    QDoubleSpinBox *timeSpinBox;
    QFrame *line_4;
    QHBoxLayout *horizontalLayout_19;
    QVBoxLayout *verticalLayout_35;
    QLabel *label_47;
    QHBoxLayout *horizontalLayout_54;
    QLCDNumber *phaseLevel;
    QDial *SigPhaSeterToAdd;
    QFrame *line_6;
    QVBoxLayout *verticalLayout_75;
    QLabel *label_49;
    QHBoxLayout *horizontalLayout_21;
    QLCDNumber *aproxLevel;
    QDial *SigAproxLvL;
    QFrame *line_32;
    QHBoxLayout *horizontalLayout_55;
    QLabel *label_48;
    QVBoxLayout *verticalLayout_74;
    QRadioButton *setSinc;
    QRadioButton *setSin;
    QRadioButton *setSquare;
    QRadioButton *setSqAprox;
    QRadioButton *setTri;
    QRadioButton *setTriAprox;
    QRadioButton *setSaw;
    QRadioButton *setSawAprox;
    QFrame *line_5;
    QVBoxLayout *verticalLayout_37;
    QLabel *label_10;
    QRadioButton *radioButton_7;
    QRadioButton *radioButton_9;
    QPushButton *addSignalButton;
    QFrame *line_33;
    QVBoxLayout *verticalLayout_76;
    QTableView *tableView;
    QCustomPlot *sigPlot;
    QHBoxLayout *horizontalLayout_56;
    QSpacerItem *horizontalSpacer_16;
    QPushButton *deleteSelected_3;
    QWidget *tab_19;
    QHBoxLayout *horizontalLayout_58;
    QGroupBox *groupBox_19;
    QVBoxLayout *verticalLayout_79;
    QListWidget *listSpectralAnalisis;
    QPushButton *catchIFFTButton;
    QPushButton *catchFFTButton;
    QVBoxLayout *verticalLayout_36;
    QFrame *frame_16;
    QVBoxLayout *verticalLayout_80;
    QCustomPlot *fftPlot;
    QHBoxLayout *horizontalLayout_24;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *catchSpectral;
    QWidget *tab_20;
    QHBoxLayout *horizontalLayout_17;
    QGroupBox *groupBox_21;
    QVBoxLayout *verticalLayout_34;
    QHBoxLayout *horizontalLayout_18;
    QListWidget *listCorelationX;
    QListWidget *listCorelationY;
    QPushButton *rxyCheck;
    QFrame *frame_18;
    QVBoxLayout *verticalLayout_84;
    QCustomPlot *rxyPlot;
    QWidget *tab_21;
    QHBoxLayout *horizontalLayout_62;
    QGroupBox *groupBox_22;
    QVBoxLayout *verticalLayout_85;
    QListWidget *listOperationsA;
    QHBoxLayout *horizontalLayout_20;
    QPushButton *additionSignal;
    QPushButton *subtractionSignal;
    QPushButton *multiplicationSignal;
    QPushButton *divisionSignal;
    QListWidget *listOperationsB;
    QFrame *frame_19;
    QHBoxLayout *horizontalLayout_63;
    QCustomPlot *opPlot;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *groupBox_24;
    QVBoxLayout *verticalLayout_90;
    QListWidget *listWindowing;
    QPushButton *windowingHamming;
    QPushButton *windowingHanning;
    QPushButton *windowingBlackman;
    QPushButton *windowingBlackmanHarris;
    QPushButton *windowingBarlettHanning;
    QFrame *frame;
    QVBoxLayout *verticalLayout_39;
    QCustomPlot *winPlot;
    QWidget *tab_22;
    QHBoxLayout *horizontalLayout_64;
    QGroupBox *groupBox_23;
    QVBoxLayout *verticalLayout_88;
    QFrame *frame_20;
    QVBoxLayout *verticalLayout_89;
    QCustomPlot *otherPlot;
    QWidget *settingsPage;
    QVBoxLayout *verticalLayout_52;
    QHBoxLayout *settingsControlLayout;
    QLabel *settingsTitleLabel;
    QSpacerItem *settingsControlLayoutSpacer;
    QPushButton *settingsBackButton;
    QPushButton *settingsCloseButton;
    QHBoxLayout *settingsFooterLayout;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1343, 912);
        MainWindow->setMinimumSize(QSize(1024, 768));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/aPLico"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setMinimumSize(QSize(1024, 768));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        mainPage = new QWidget();
        mainPage->setObjectName(QStringLiteral("mainPage"));
        horizontalLayout = new QHBoxLayout(mainPage);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        mainLayout = new QVBoxLayout();
        mainLayout->setSpacing(6);
        mainLayout->setObjectName(QStringLiteral("mainLayout"));
        mainControlLayout = new QHBoxLayout();
        mainControlLayout->setSpacing(6);
        mainControlLayout->setObjectName(QStringLiteral("mainControlLayout"));
        mainControlLayout->setSizeConstraint(QLayout::SetMinimumSize);
        mainTitleLabel = new QLabel(mainPage);
        mainTitleLabel->setObjectName(QStringLiteral("mainTitleLabel"));

        mainControlLayout->addWidget(mainTitleLabel);

        mainControlLayoutSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        mainControlLayout->addItem(mainControlLayoutSpacer);

        mainBrainButton = new QPushButton(mainPage);
        mainBrainButton->setObjectName(QStringLiteral("mainBrainButton"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/pictures/pictures/brain.png"), QSize(), QIcon::Normal, QIcon::Off);
        mainBrainButton->setIcon(icon1);

        mainControlLayout->addWidget(mainBrainButton);

        mainSettingsButton = new QPushButton(mainPage);
        mainSettingsButton->setObjectName(QStringLiteral("mainSettingsButton"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/pictures/pictures/settings.png"), QSize(), QIcon::Normal, QIcon::Off);
        mainSettingsButton->setIcon(icon2);

        mainControlLayout->addWidget(mainSettingsButton);

        mainCloseButton = new QPushButton(mainPage);
        mainCloseButton->setObjectName(QStringLiteral("mainCloseButton"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/pictures/pictures/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        mainCloseButton->setIcon(icon3);

        mainControlLayout->addWidget(mainCloseButton);


        mainLayout->addLayout(mainControlLayout);

        mainContentLayout = new QHBoxLayout();
        mainContentLayout->setSpacing(6);
        mainContentLayout->setObjectName(QStringLiteral("mainContentLayout"));
        mainLFOLayout = new QVBoxLayout();
        mainLFOLayout->setSpacing(6);
        mainLFOLayout->setObjectName(QStringLiteral("mainLFOLayout"));
        mainLFOLayout->setContentsMargins(-1, -1, -1, 0);
        mainLFOSliderLayout = new QHBoxLayout();
        mainLFOSliderLayout->setSpacing(6);
        mainLFOSliderLayout->setObjectName(QStringLiteral("mainLFOSliderLayout"));
        mainLFOSliderLayout->setContentsMargins(-1, -1, 0, 0);
        onlyLFOLayout = new QVBoxLayout();
        onlyLFOLayout->setSpacing(6);
        onlyLFOLayout->setObjectName(QStringLiteral("onlyLFOLayout"));
        onlyLFOLayout->setContentsMargins(-1, -1, -1, 0);
        lfo_1GroupBox = new QGroupBox(mainPage);
        lfo_1GroupBox->setObjectName(QStringLiteral("lfo_1GroupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lfo_1GroupBox->sizePolicy().hasHeightForWidth());
        lfo_1GroupBox->setSizePolicy(sizePolicy);
        lfo_1GroupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
" } "));
        verticalLayout_19 = new QVBoxLayout(lfo_1GroupBox);
        verticalLayout_19->setSpacing(6);
        verticalLayout_19->setContentsMargins(11, 11, 11, 11);
        verticalLayout_19->setObjectName(QStringLiteral("verticalLayout_19"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(-1, -1, -1, 0);
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(-1, -1, 0, -1);
        lfo1AmountDial = new QDial(lfo_1GroupBox);
        lfo1AmountDial->setObjectName(QStringLiteral("lfo1AmountDial"));
        lfo1AmountDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));
        lfo1AmountDial->setMinimum(1);
        lfo1AmountDial->setMaximum(100);

        verticalLayout_4->addWidget(lfo1AmountDial);

        label_13 = new QLabel(lfo_1GroupBox);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_13);


        horizontalLayout_12->addLayout(verticalLayout_4);

        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setSpacing(6);
        verticalLayout_18->setObjectName(QStringLiteral("verticalLayout_18"));
        lfo1RateDial = new QDial(lfo_1GroupBox);
        lfo1RateDial->setObjectName(QStringLiteral("lfo1RateDial"));
        lfo1RateDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));
        lfo1RateDial->setMinimum(2000);
        lfo1RateDial->setMaximum(20000);
        lfo1RateDial->setSingleStep(500);
        lfo1RateDial->setValue(2000);

        verticalLayout_18->addWidget(lfo1RateDial);

        label_14 = new QLabel(lfo_1GroupBox);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setAlignment(Qt::AlignCenter);

        verticalLayout_18->addWidget(label_14);


        horizontalLayout_12->addLayout(verticalLayout_18);


        verticalLayout_19->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        horizontalLayout_13->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_4);

        lfo_1ShapeSaw = new QPushButton(lfo_1GroupBox);
        lfo_1ShapeSaw->setObjectName(QStringLiteral("lfo_1ShapeSaw"));
        lfo_1ShapeSaw->setMinimumSize(QSize(40, 40));
        lfo_1ShapeSaw->setMaximumSize(QSize(25, 16777215));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/pictures/pictures/saw.png"), QSize(), QIcon::Normal, QIcon::Off);
        lfo_1ShapeSaw->setIcon(icon4);
        lfo_1ShapeSaw->setIconSize(QSize(40, 16));
        lfo_1ShapeSaw->setCheckable(true);
        lfo_1ShapeSaw->setChecked(false);

        horizontalLayout_13->addWidget(lfo_1ShapeSaw);

        lfo_1ShapeSquare = new QPushButton(lfo_1GroupBox);
        lfo_1ShapeSquare->setObjectName(QStringLiteral("lfo_1ShapeSquare"));
        lfo_1ShapeSquare->setMinimumSize(QSize(40, 40));
        lfo_1ShapeSquare->setMaximumSize(QSize(25, 16777215));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/pictures/pictures/quad.png"), QSize(), QIcon::Normal, QIcon::Off);
        lfo_1ShapeSquare->setIcon(icon5);
        lfo_1ShapeSquare->setIconSize(QSize(40, 16));
        lfo_1ShapeSquare->setCheckable(true);

        horizontalLayout_13->addWidget(lfo_1ShapeSquare);

        lfo_1ShapeSin = new QPushButton(lfo_1GroupBox);
        lfo_1ShapeSin->setObjectName(QStringLiteral("lfo_1ShapeSin"));
        lfo_1ShapeSin->setMinimumSize(QSize(40, 40));
        lfo_1ShapeSin->setMaximumSize(QSize(25, 16777215));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/pictures/pictures/sine.png"), QSize(), QIcon::Normal, QIcon::Off);
        lfo_1ShapeSin->setIcon(icon6);
        lfo_1ShapeSin->setIconSize(QSize(40, 20));
        lfo_1ShapeSin->setCheckable(true);

        horizontalLayout_13->addWidget(lfo_1ShapeSin);

        lfo_1ShapeNoise = new QPushButton(lfo_1GroupBox);
        lfo_1ShapeNoise->setObjectName(QStringLiteral("lfo_1ShapeNoise"));
        lfo_1ShapeNoise->setMinimumSize(QSize(40, 40));
        lfo_1ShapeNoise->setMaximumSize(QSize(25, 16777215));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/pictures/pictures/noise.png"), QSize(), QIcon::Normal, QIcon::Off);
        lfo_1ShapeNoise->setIcon(icon7);
        lfo_1ShapeNoise->setIconSize(QSize(40, 20));
        lfo_1ShapeNoise->setCheckable(true);

        horizontalLayout_13->addWidget(lfo_1ShapeNoise);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_5);


        verticalLayout_19->addLayout(horizontalLayout_13);


        onlyLFOLayout->addWidget(lfo_1GroupBox);

        lfo_2GroupBox = new QGroupBox(mainPage);
        lfo_2GroupBox->setObjectName(QStringLiteral("lfo_2GroupBox"));
        sizePolicy.setHeightForWidth(lfo_2GroupBox->sizePolicy().hasHeightForWidth());
        lfo_2GroupBox->setSizePolicy(sizePolicy);
        lfo_2GroupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
" } "));
        verticalLayout_17 = new QVBoxLayout(lfo_2GroupBox);
        verticalLayout_17->setSpacing(6);
        verticalLayout_17->setContentsMargins(11, 11, 11, 11);
        verticalLayout_17->setObjectName(QStringLiteral("verticalLayout_17"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(-1, -1, -1, 0);
        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setSpacing(6);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));
        verticalLayout_15->setContentsMargins(-1, -1, 0, -1);
        lfo2AmountDial = new QDial(lfo_2GroupBox);
        lfo2AmountDial->setObjectName(QStringLiteral("lfo2AmountDial"));
        lfo2AmountDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));
        lfo2AmountDial->setMinimum(1);
        lfo2AmountDial->setMaximum(100);

        verticalLayout_15->addWidget(lfo2AmountDial);

        label_11 = new QLabel(lfo_2GroupBox);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setAlignment(Qt::AlignCenter);

        verticalLayout_15->addWidget(label_11);


        horizontalLayout_10->addLayout(verticalLayout_15);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setSpacing(6);
        verticalLayout_16->setObjectName(QStringLiteral("verticalLayout_16"));
        lfo2RateDial = new QDial(lfo_2GroupBox);
        lfo2RateDial->setObjectName(QStringLiteral("lfo2RateDial"));
        lfo2RateDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));
        lfo2RateDial->setMinimum(2000);
        lfo2RateDial->setMaximum(20000);
        lfo2RateDial->setSingleStep(500);

        verticalLayout_16->addWidget(lfo2RateDial);

        label_12 = new QLabel(lfo_2GroupBox);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(label_12);


        horizontalLayout_10->addLayout(verticalLayout_16);


        verticalLayout_17->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_7);

        lfo_2ShapeSaw = new QPushButton(lfo_2GroupBox);
        lfo_2ShapeSaw->setObjectName(QStringLiteral("lfo_2ShapeSaw"));
        lfo_2ShapeSaw->setMinimumSize(QSize(40, 40));
        lfo_2ShapeSaw->setMaximumSize(QSize(25, 16777215));
        lfo_2ShapeSaw->setIcon(icon4);
        lfo_2ShapeSaw->setIconSize(QSize(40, 16));
        lfo_2ShapeSaw->setCheckable(true);

        horizontalLayout_11->addWidget(lfo_2ShapeSaw);

        lfo_2ShapeSquare = new QPushButton(lfo_2GroupBox);
        lfo_2ShapeSquare->setObjectName(QStringLiteral("lfo_2ShapeSquare"));
        lfo_2ShapeSquare->setMinimumSize(QSize(40, 40));
        lfo_2ShapeSquare->setMaximumSize(QSize(25, 16777215));
        lfo_2ShapeSquare->setIcon(icon5);
        lfo_2ShapeSquare->setIconSize(QSize(40, 16));
        lfo_2ShapeSquare->setCheckable(true);

        horizontalLayout_11->addWidget(lfo_2ShapeSquare);

        lfo_2ShapeSin = new QPushButton(lfo_2GroupBox);
        lfo_2ShapeSin->setObjectName(QStringLiteral("lfo_2ShapeSin"));
        lfo_2ShapeSin->setMinimumSize(QSize(40, 40));
        lfo_2ShapeSin->setMaximumSize(QSize(25, 16777215));
        lfo_2ShapeSin->setIcon(icon6);
        lfo_2ShapeSin->setIconSize(QSize(40, 20));
        lfo_2ShapeSin->setCheckable(true);

        horizontalLayout_11->addWidget(lfo_2ShapeSin);

        lfo_2ShapeNoise = new QPushButton(lfo_2GroupBox);
        lfo_2ShapeNoise->setObjectName(QStringLiteral("lfo_2ShapeNoise"));
        lfo_2ShapeNoise->setMinimumSize(QSize(40, 40));
        lfo_2ShapeNoise->setMaximumSize(QSize(25, 16777215));
        lfo_2ShapeNoise->setIcon(icon7);
        lfo_2ShapeNoise->setIconSize(QSize(40, 20));
        lfo_2ShapeNoise->setCheckable(true);

        horizontalLayout_11->addWidget(lfo_2ShapeNoise);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_6);


        verticalLayout_17->addLayout(horizontalLayout_11);


        onlyLFOLayout->addWidget(lfo_2GroupBox);


        mainLFOSliderLayout->addLayout(onlyLFOLayout);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setSpacing(6);
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        verticalLayout_12->setContentsMargins(-1, -1, 0, -1);
        lfo_1DestGroupBox = new QGroupBox(mainPage);
        lfo_1DestGroupBox->setObjectName(QStringLiteral("lfo_1DestGroupBox"));
        lfo_1DestGroupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
" } "));
        verticalLayout_31 = new QVBoxLayout(lfo_1DestGroupBox);
        verticalLayout_31->setSpacing(6);
        verticalLayout_31->setContentsMargins(11, 11, 11, 11);
        verticalLayout_31->setObjectName(QStringLiteral("verticalLayout_31"));
        radioButton_5 = new QRadioButton(lfo_1DestGroupBox);
        radioButton_5->setObjectName(QStringLiteral("radioButton_5"));
        radioButton_5->setChecked(true);

        verticalLayout_31->addWidget(radioButton_5);

        radioButton_6 = new QRadioButton(lfo_1DestGroupBox);
        radioButton_6->setObjectName(QStringLiteral("radioButton_6"));

        verticalLayout_31->addWidget(radioButton_6);


        verticalLayout_12->addWidget(lfo_1DestGroupBox);

        lfoMixGroupBox = new QGroupBox(mainPage);
        lfoMixGroupBox->setObjectName(QStringLiteral("lfoMixGroupBox"));
        lfoMixGroupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"padding-top:12px;\n"
" } "));
        lfoMixGroupBox->setFlat(false);
        verticalLayout_13 = new QVBoxLayout(lfoMixGroupBox);
        verticalLayout_13->setSpacing(6);
        verticalLayout_13->setContentsMargins(11, 11, 11, 11);
        verticalLayout_13->setObjectName(QStringLiteral("verticalLayout_13"));
        checkBox = new QCheckBox(lfoMixGroupBox);
        checkBox->setObjectName(QStringLiteral("checkBox"));

        verticalLayout_13->addWidget(checkBox);

        radioButton = new QRadioButton(lfoMixGroupBox);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setEnabled(false);

        verticalLayout_13->addWidget(radioButton);

        radioButton_2 = new QRadioButton(lfoMixGroupBox);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setEnabled(false);

        verticalLayout_13->addWidget(radioButton_2);

        radioButton_3 = new QRadioButton(lfoMixGroupBox);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setEnabled(false);

        verticalLayout_13->addWidget(radioButton_3);

        radioButton_4 = new QRadioButton(lfoMixGroupBox);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setEnabled(false);

        verticalLayout_13->addWidget(radioButton_4);


        verticalLayout_12->addWidget(lfoMixGroupBox);

        lfo_2DestGroupBox = new QGroupBox(mainPage);
        lfo_2DestGroupBox->setObjectName(QStringLiteral("lfo_2DestGroupBox"));
        lfo_2DestGroupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
" } "));
        verticalLayout_32 = new QVBoxLayout(lfo_2DestGroupBox);
        verticalLayout_32->setSpacing(6);
        verticalLayout_32->setContentsMargins(11, 11, 11, 11);
        verticalLayout_32->setObjectName(QStringLiteral("verticalLayout_32"));
        radioButton_8 = new QRadioButton(lfo_2DestGroupBox);
        radioButton_8->setObjectName(QStringLiteral("radioButton_8"));
        radioButton_8->setChecked(true);

        verticalLayout_32->addWidget(radioButton_8);

        radioButton_10 = new QRadioButton(lfo_2DestGroupBox);
        radioButton_10->setObjectName(QStringLiteral("radioButton_10"));

        verticalLayout_32->addWidget(radioButton_10);


        verticalLayout_12->addWidget(lfo_2DestGroupBox);


        mainLFOSliderLayout->addLayout(verticalLayout_12);

        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setSpacing(6);
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        verticalLayout_14->setContentsMargins(-1, -1, 0, -1);
        label_7 = new QLabel(mainPage);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMaximumSize(QSize(16777215, 16777215));
        label_7->setLineWidth(2);

        verticalLayout_14->addWidget(label_7);

        lfoSlider = new QSlider(mainPage);
        lfoSlider->setObjectName(QStringLiteral("lfoSlider"));
        lfoSlider->setEnabled(false);
        lfoSlider->setMinimumSize(QSize(30, 0));
        lfoSlider->setMinimum(-100);
        lfoSlider->setMaximum(100);
        lfoSlider->setOrientation(Qt::Vertical);

        verticalLayout_14->addWidget(lfoSlider);


        mainLFOSliderLayout->addLayout(verticalLayout_14);


        mainLFOLayout->addLayout(mainLFOSliderLayout);

        filtherGroupBox = new QGroupBox(mainPage);
        filtherGroupBox->setObjectName(QStringLiteral("filtherGroupBox"));
        sizePolicy.setHeightForWidth(filtherGroupBox->sizePolicy().hasHeightForWidth());
        filtherGroupBox->setSizePolicy(sizePolicy);
        filtherGroupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
" } "));
        verticalLayout_5 = new QVBoxLayout(filtherGroupBox);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, -1, -1, 0);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(-1, -1, 0, -1);
        filResoDial = new QDial(filtherGroupBox);
        filResoDial->setObjectName(QStringLiteral("filResoDial"));
        filResoDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));

        verticalLayout_3->addWidget(filResoDial);

        label_2 = new QLabel(filtherGroupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_2);


        horizontalLayout_3->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        filCutOffDial = new QDial(filtherGroupBox);
        filCutOffDial->setObjectName(QStringLiteral("filCutOffDial"));
        filCutOffDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));

        verticalLayout_2->addWidget(filCutOffDial);

        label = new QLabel(filtherGroupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label);


        horizontalLayout_3->addLayout(verticalLayout_2);


        verticalLayout_5->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        pushButton_3 = new QPushButton(filtherGroupBox);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(40, 40));
        pushButton_3->setMaximumSize(QSize(25, 16777215));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/pictures/pictures/filter_lowpass.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_3->setIcon(icon8);
        pushButton_3->setIconSize(QSize(40, 20));

        horizontalLayout_4->addWidget(pushButton_3);

        pushButton_2 = new QPushButton(filtherGroupBox);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(40, 40));
        pushButton_2->setMaximumSize(QSize(25, 16777215));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/pictures/pictures/filter_bandpass.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_2->setIcon(icon9);
        pushButton_2->setIconSize(QSize(40, 20));

        horizontalLayout_4->addWidget(pushButton_2);

        pushButton = new QPushButton(filtherGroupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMinimumSize(QSize(40, 40));
        pushButton->setMaximumSize(QSize(25, 16777215));
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/pictures/pictures/filter_hipass.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon10);
        pushButton->setIconSize(QSize(40, 20));

        horizontalLayout_4->addWidget(pushButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);


        verticalLayout_5->addLayout(horizontalLayout_4);


        mainLFOLayout->addWidget(filtherGroupBox);

        groupBox = new QGroupBox(mainPage);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMinimumSize(QSize(0, 100));
        groupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
" } "));
        horizontalLayout_14 = new QHBoxLayout(groupBox);
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        keyC = new QPushButton(groupBox);
        keyC->setObjectName(QStringLiteral("keyC"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(keyC->sizePolicy().hasHeightForWidth());
        keyC->setSizePolicy(sizePolicy1);
        keyC->setMinimumSize(QSize(50, 50));
        keyC->setMaximumSize(QSize(20, 100));
        keyC->setLayoutDirection(Qt::LeftToRight);
        keyC->setStyleSheet(QStringLiteral(""));

        horizontalLayout_14->addWidget(keyC);

        keyD = new QPushButton(groupBox);
        keyD->setObjectName(QStringLiteral("keyD"));
        keyD->setMinimumSize(QSize(50, 50));
        keyD->setMaximumSize(QSize(20, 100));
        keyD->setStyleSheet(QStringLiteral(""));

        horizontalLayout_14->addWidget(keyD);

        keyE = new QPushButton(groupBox);
        keyE->setObjectName(QStringLiteral("keyE"));
        keyE->setMinimumSize(QSize(50, 50));
        keyE->setMaximumSize(QSize(20, 100));
        keyE->setStyleSheet(QStringLiteral(""));

        horizontalLayout_14->addWidget(keyE);

        keyF = new QPushButton(groupBox);
        keyF->setObjectName(QStringLiteral("keyF"));
        keyF->setMinimumSize(QSize(50, 50));
        keyF->setMaximumSize(QSize(20, 100));
        keyF->setStyleSheet(QStringLiteral(""));

        horizontalLayout_14->addWidget(keyF);

        keyG = new QPushButton(groupBox);
        keyG->setObjectName(QStringLiteral("keyG"));
        keyG->setMinimumSize(QSize(50, 50));
        keyG->setMaximumSize(QSize(20, 100));
        keyG->setStyleSheet(QStringLiteral(""));

        horizontalLayout_14->addWidget(keyG);

        keyA = new QPushButton(groupBox);
        keyA->setObjectName(QStringLiteral("keyA"));
        keyA->setMinimumSize(QSize(50, 50));
        keyA->setMaximumSize(QSize(20, 100));
        keyA->setStyleSheet(QStringLiteral(""));

        horizontalLayout_14->addWidget(keyA);

        keyH = new QPushButton(groupBox);
        keyH->setObjectName(QStringLiteral("keyH"));
        keyH->setMinimumSize(QSize(50, 50));
        keyH->setMaximumSize(QSize(20, 100));
        keyH->setStyleSheet(QStringLiteral(""));

        horizontalLayout_14->addWidget(keyH);


        mainLFOLayout->addWidget(groupBox);


        mainContentLayout->addLayout(mainLFOLayout);

        line = new QFrame(mainPage);
        line->setObjectName(QStringLiteral("line"));
        line->setStyleSheet(QLatin1String("Line {\n"
"border: 2px solid gray; \n"
"}"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        mainContentLayout->addWidget(line);

        mainOSCLayout = new QVBoxLayout();
        mainOSCLayout->setSpacing(6);
        mainOSCLayout->setObjectName(QStringLiteral("mainOSCLayout"));
        mainOSCLayout->setContentsMargins(-1, 0, -1, -1);
        mainOSCSliderLayout = new QHBoxLayout();
        mainOSCSliderLayout->setSpacing(6);
        mainOSCSliderLayout->setObjectName(QStringLiteral("mainOSCSliderLayout"));
        mainOSCSliderLayout->setContentsMargins(-1, -1, 0, -1);
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(-1, -1, 0, 0);
        osc_1GroupBox = new QGroupBox(mainPage);
        osc_1GroupBox->setObjectName(QStringLiteral("osc_1GroupBox"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(osc_1GroupBox->sizePolicy().hasHeightForWidth());
        osc_1GroupBox->setSizePolicy(sizePolicy2);
        osc_1GroupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
"     padding-right:40px;\n"
" } "));
        horizontalLayout_5 = new QHBoxLayout(osc_1GroupBox);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(-1, -1, 0, 0);
        osc1CoarseDial = new QDial(osc_1GroupBox);
        osc1CoarseDial->setObjectName(QStringLiteral("osc1CoarseDial"));
        osc1CoarseDial->setMinimumSize(QSize(150, 150));
        osc1CoarseDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));
        osc1CoarseDial->setMinimum(-150);
        osc1CoarseDial->setMaximum(150);

        verticalLayout_8->addWidget(osc1CoarseDial);

        label_3 = new QLabel(osc_1GroupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(label_3);

        osc1FineDial = new QDial(osc_1GroupBox);
        osc1FineDial->setObjectName(QStringLiteral("osc1FineDial"));
        osc1FineDial->setMinimumSize(QSize(150, 150));
        osc1FineDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));
        osc1FineDial->setMinimum(-100);
        osc1FineDial->setMaximum(100);

        verticalLayout_8->addWidget(osc1FineDial);

        label_4 = new QLabel(osc_1GroupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(label_4);


        horizontalLayout_5->addLayout(verticalLayout_8);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(-1, -1, 0, -1);
        osc1Saw = new QPushButton(osc_1GroupBox);
        osc1Saw->setObjectName(QStringLiteral("osc1Saw"));
        osc1Saw->setMinimumSize(QSize(40, 40));
        osc1Saw->setMaximumSize(QSize(25, 16777215));
        osc1Saw->setIcon(icon4);
        osc1Saw->setIconSize(QSize(40, 16));
        osc1Saw->setCheckable(true);

        verticalLayout_7->addWidget(osc1Saw);

        osc1Squa = new QPushButton(osc_1GroupBox);
        osc1Squa->setObjectName(QStringLiteral("osc1Squa"));
        osc1Squa->setMinimumSize(QSize(40, 40));
        osc1Squa->setMaximumSize(QSize(25, 16777215));
        osc1Squa->setIcon(icon5);
        osc1Squa->setIconSize(QSize(40, 12));
        osc1Squa->setCheckable(true);

        verticalLayout_7->addWidget(osc1Squa);

        osc1Sin = new QPushButton(osc_1GroupBox);
        osc1Sin->setObjectName(QStringLiteral("osc1Sin"));
        osc1Sin->setMinimumSize(QSize(40, 40));
        osc1Sin->setMaximumSize(QSize(25, 25));
        osc1Sin->setIcon(icon6);
        osc1Sin->setIconSize(QSize(40, 16));
        osc1Sin->setCheckable(true);

        verticalLayout_7->addWidget(osc1Sin);

        osc1Nois = new QPushButton(osc_1GroupBox);
        osc1Nois->setObjectName(QStringLiteral("osc1Nois"));
        osc1Nois->setMinimumSize(QSize(40, 40));
        osc1Nois->setMaximumSize(QSize(25, 16777215));
        osc1Nois->setIcon(icon7);
        osc1Nois->setIconSize(QSize(40, 20));
        osc1Nois->setCheckable(true);

        verticalLayout_7->addWidget(osc1Nois);


        horizontalLayout_5->addLayout(verticalLayout_7);


        verticalLayout_6->addWidget(osc_1GroupBox);

        osc_2GroupBox = new QGroupBox(mainPage);
        osc_2GroupBox->setObjectName(QStringLiteral("osc_2GroupBox"));
        sizePolicy2.setHeightForWidth(osc_2GroupBox->sizePolicy().hasHeightForWidth());
        osc_2GroupBox->setSizePolicy(sizePolicy2);
        osc_2GroupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
"     padding-right:40px;\n"
" } "));
        horizontalLayout_6 = new QHBoxLayout(osc_2GroupBox);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        verticalLayout_10->setContentsMargins(-1, -1, 0, 0);
        osc2CoarseDial = new QDial(osc_2GroupBox);
        osc2CoarseDial->setObjectName(QStringLiteral("osc2CoarseDial"));
        osc2CoarseDial->setMinimumSize(QSize(150, 150));
        osc2CoarseDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));
        osc2CoarseDial->setMinimum(-150);
        osc2CoarseDial->setMaximum(150);

        verticalLayout_10->addWidget(osc2CoarseDial);

        label_5 = new QLabel(osc_2GroupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(label_5);

        osc2FineDial = new QDial(osc_2GroupBox);
        osc2FineDial->setObjectName(QStringLiteral("osc2FineDial"));
        osc2FineDial->setMinimumSize(QSize(150, 150));
        osc2FineDial->setStyleSheet(QLatin1String("QDial {\n"
"	min-width:150px;\n"
"	min-height:150px;\n"
"}"));
        osc2FineDial->setMinimum(-100);
        osc2FineDial->setMaximum(100);

        verticalLayout_10->addWidget(osc2FineDial);

        label_6 = new QLabel(osc_2GroupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setLayoutDirection(Qt::LeftToRight);
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(label_6);


        horizontalLayout_6->addLayout(verticalLayout_10);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(-1, -1, 0, -1);
        osc2Saw = new QPushButton(osc_2GroupBox);
        osc2Saw->setObjectName(QStringLiteral("osc2Saw"));
        osc2Saw->setMinimumSize(QSize(40, 40));
        osc2Saw->setMaximumSize(QSize(25, 16777215));
        osc2Saw->setIcon(icon4);
        osc2Saw->setIconSize(QSize(40, 16));
        osc2Saw->setCheckable(true);

        verticalLayout_9->addWidget(osc2Saw);

        osc2Squa = new QPushButton(osc_2GroupBox);
        osc2Squa->setObjectName(QStringLiteral("osc2Squa"));
        osc2Squa->setMinimumSize(QSize(40, 40));
        osc2Squa->setMaximumSize(QSize(25, 16777215));
        osc2Squa->setIcon(icon5);
        osc2Squa->setIconSize(QSize(40, 16));
        osc2Squa->setCheckable(true);

        verticalLayout_9->addWidget(osc2Squa);

        osc2Sin = new QPushButton(osc_2GroupBox);
        osc2Sin->setObjectName(QStringLiteral("osc2Sin"));
        osc2Sin->setMinimumSize(QSize(40, 40));
        osc2Sin->setMaximumSize(QSize(25, 16777215));
        osc2Sin->setIcon(icon6);
        osc2Sin->setIconSize(QSize(40, 20));
        osc2Sin->setCheckable(true);

        verticalLayout_9->addWidget(osc2Sin);

        osc2Nois = new QPushButton(osc_2GroupBox);
        osc2Nois->setObjectName(QStringLiteral("osc2Nois"));
        osc2Nois->setMinimumSize(QSize(40, 40));
        osc2Nois->setMaximumSize(QSize(25, 16777215));
        osc2Nois->setIcon(icon7);
        osc2Nois->setIconSize(QSize(40, 20));
        osc2Nois->setCheckable(true);

        verticalLayout_9->addWidget(osc2Nois);


        horizontalLayout_6->addLayout(verticalLayout_9);


        verticalLayout_6->addWidget(osc_2GroupBox);


        mainOSCSliderLayout->addLayout(verticalLayout_6);

        verticalLayout_30 = new QVBoxLayout();
        verticalLayout_30->setSpacing(6);
        verticalLayout_30->setObjectName(QStringLiteral("verticalLayout_30"));
        verticalLayout_30->setContentsMargins(-1, -1, 0, -1);
        label_9 = new QLabel(mainPage);
        label_9->setObjectName(QStringLiteral("label_9"));

        verticalLayout_30->addWidget(label_9);

        oscSlider = new QSlider(mainPage);
        oscSlider->setObjectName(QStringLiteral("oscSlider"));
        oscSlider->setMinimumSize(QSize(30, 0));
        oscSlider->setStyleSheet(QLatin1String("QSlider {\n"
"	border: 1px solid #000000;\n"
"}"));
        oscSlider->setMinimum(0);
        oscSlider->setMaximum(100);
        oscSlider->setValue(50);
        oscSlider->setOrientation(Qt::Vertical);
        oscSlider->setInvertedAppearance(true);
        oscSlider->setInvertedControls(false);

        verticalLayout_30->addWidget(oscSlider);


        mainOSCSliderLayout->addLayout(verticalLayout_30);


        mainOSCLayout->addLayout(mainOSCSliderLayout);


        mainContentLayout->addLayout(mainOSCLayout);

        line_2 = new QFrame(mainPage);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setStyleSheet(QLatin1String("Line {\n"
"border: 2px solid gray; \n"
"}"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        mainContentLayout->addWidget(line_2);

        mainENVLayout = new QVBoxLayout();
        mainENVLayout->setSpacing(6);
        mainENVLayout->setObjectName(QStringLiteral("mainENVLayout"));
        mainENVLayout->setContentsMargins(-1, -1, -1, 0);
        ampEnvGroupBox = new QGroupBox(mainPage);
        ampEnvGroupBox->setObjectName(QStringLiteral("ampEnvGroupBox"));
        sizePolicy2.setHeightForWidth(ampEnvGroupBox->sizePolicy().hasHeightForWidth());
        ampEnvGroupBox->setSizePolicy(sizePolicy2);
        ampEnvGroupBox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
" } "));
        verticalLayout_20 = new QVBoxLayout(ampEnvGroupBox);
        verticalLayout_20->setSpacing(6);
        verticalLayout_20->setContentsMargins(11, 11, 11, 11);
        verticalLayout_20->setObjectName(QStringLiteral("verticalLayout_20"));
        ampEnvPlot = new QCustomPlot(ampEnvGroupBox);
        ampEnvPlot->setObjectName(QStringLiteral("ampEnvPlot"));
        ampEnvPlot->setMinimumSize(QSize(0, 200));

        verticalLayout_20->addWidget(ampEnvPlot);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        horizontalLayout_15->setContentsMargins(-1, -1, 0, -1);
        verticalLayout_21 = new QVBoxLayout();
        verticalLayout_21->setSpacing(6);
        verticalLayout_21->setObjectName(QStringLiteral("verticalLayout_21"));
        ampEnvelopeAtkDial = new QDial(ampEnvGroupBox);
        ampEnvelopeAtkDial->setObjectName(QStringLiteral("ampEnvelopeAtkDial"));
        ampEnvelopeAtkDial->setMaximum(250);

        verticalLayout_21->addWidget(ampEnvelopeAtkDial);

        label_15 = new QLabel(ampEnvGroupBox);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setAlignment(Qt::AlignCenter);

        verticalLayout_21->addWidget(label_15);


        horizontalLayout_15->addLayout(verticalLayout_21);

        verticalLayout_22 = new QVBoxLayout();
        verticalLayout_22->setSpacing(6);
        verticalLayout_22->setObjectName(QStringLiteral("verticalLayout_22"));
        verticalLayout_22->setContentsMargins(-1, -1, 0, -1);
        ampEnvelopeDecDial = new QDial(ampEnvGroupBox);
        ampEnvelopeDecDial->setObjectName(QStringLiteral("ampEnvelopeDecDial"));
        ampEnvelopeDecDial->setMaximum(200);

        verticalLayout_22->addWidget(ampEnvelopeDecDial);

        label_16 = new QLabel(ampEnvGroupBox);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setAlignment(Qt::AlignCenter);

        verticalLayout_22->addWidget(label_16);


        horizontalLayout_15->addLayout(verticalLayout_22);

        verticalLayout_23 = new QVBoxLayout();
        verticalLayout_23->setSpacing(6);
        verticalLayout_23->setObjectName(QStringLiteral("verticalLayout_23"));
        verticalLayout_23->setContentsMargins(-1, -1, 0, -1);
        ampEnvelopeSusDial = new QDial(ampEnvGroupBox);
        ampEnvelopeSusDial->setObjectName(QStringLiteral("ampEnvelopeSusDial"));
        ampEnvelopeSusDial->setMinimum(1);
        ampEnvelopeSusDial->setMaximum(100);

        verticalLayout_23->addWidget(ampEnvelopeSusDial);

        label_17 = new QLabel(ampEnvGroupBox);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setAlignment(Qt::AlignCenter);

        verticalLayout_23->addWidget(label_17);


        horizontalLayout_15->addLayout(verticalLayout_23);

        verticalLayout_24 = new QVBoxLayout();
        verticalLayout_24->setSpacing(6);
        verticalLayout_24->setObjectName(QStringLiteral("verticalLayout_24"));
        verticalLayout_24->setContentsMargins(-1, -1, 0, -1);
        ampEnvelopeRelDial = new QDial(ampEnvGroupBox);
        ampEnvelopeRelDial->setObjectName(QStringLiteral("ampEnvelopeRelDial"));
        ampEnvelopeRelDial->setMinimum(1);
        ampEnvelopeRelDial->setMaximum(200);

        verticalLayout_24->addWidget(ampEnvelopeRelDial);

        label_18 = new QLabel(ampEnvGroupBox);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setAlignment(Qt::AlignCenter);

        verticalLayout_24->addWidget(label_18);


        horizontalLayout_15->addLayout(verticalLayout_24);


        verticalLayout_20->addLayout(horizontalLayout_15);


        mainENVLayout->addWidget(ampEnvGroupBox);

        filEnvGroupbox = new QGroupBox(mainPage);
        filEnvGroupbox->setObjectName(QStringLiteral("filEnvGroupbox"));
        sizePolicy2.setHeightForWidth(filEnvGroupbox->sizePolicy().hasHeightForWidth());
        filEnvGroupbox->setSizePolicy(sizePolicy2);
        filEnvGroupbox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
" } "));
        verticalLayout_29 = new QVBoxLayout(filEnvGroupbox);
        verticalLayout_29->setSpacing(6);
        verticalLayout_29->setContentsMargins(11, 11, 11, 11);
        verticalLayout_29->setObjectName(QStringLiteral("verticalLayout_29"));
        fillEnvPlot = new QCustomPlot(filEnvGroupbox);
        fillEnvPlot->setObjectName(QStringLiteral("fillEnvPlot"));
        fillEnvPlot->setMinimumSize(QSize(0, 200));

        verticalLayout_29->addWidget(fillEnvPlot);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        horizontalLayout_16->setContentsMargins(-1, -1, 0, -1);
        verticalLayout_25 = new QVBoxLayout();
        verticalLayout_25->setSpacing(6);
        verticalLayout_25->setObjectName(QStringLiteral("verticalLayout_25"));
        filEnvelopeAtkDial = new QDial(filEnvGroupbox);
        filEnvelopeAtkDial->setObjectName(QStringLiteral("filEnvelopeAtkDial"));
        filEnvelopeAtkDial->setMinimum(0);
        filEnvelopeAtkDial->setMaximum(250);
        filEnvelopeAtkDial->setNotchesVisible(false);

        verticalLayout_25->addWidget(filEnvelopeAtkDial);

        label_19 = new QLabel(filEnvGroupbox);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setAlignment(Qt::AlignCenter);

        verticalLayout_25->addWidget(label_19);


        horizontalLayout_16->addLayout(verticalLayout_25);

        verticalLayout_26 = new QVBoxLayout();
        verticalLayout_26->setSpacing(6);
        verticalLayout_26->setObjectName(QStringLiteral("verticalLayout_26"));
        verticalLayout_26->setContentsMargins(-1, -1, 0, -1);
        filEnvelopeDecDial = new QDial(filEnvGroupbox);
        filEnvelopeDecDial->setObjectName(QStringLiteral("filEnvelopeDecDial"));
        filEnvelopeDecDial->setMinimum(0);
        filEnvelopeDecDial->setMaximum(200);

        verticalLayout_26->addWidget(filEnvelopeDecDial);

        label_20 = new QLabel(filEnvGroupbox);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setAlignment(Qt::AlignCenter);

        verticalLayout_26->addWidget(label_20);


        horizontalLayout_16->addLayout(verticalLayout_26);

        verticalLayout_27 = new QVBoxLayout();
        verticalLayout_27->setSpacing(6);
        verticalLayout_27->setObjectName(QStringLiteral("verticalLayout_27"));
        verticalLayout_27->setContentsMargins(-1, -1, 0, -1);
        filEnvelopeSusDial = new QDial(filEnvGroupbox);
        filEnvelopeSusDial->setObjectName(QStringLiteral("filEnvelopeSusDial"));
        filEnvelopeSusDial->setMinimum(1);
        filEnvelopeSusDial->setMaximum(100);

        verticalLayout_27->addWidget(filEnvelopeSusDial);

        label_21 = new QLabel(filEnvGroupbox);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setAlignment(Qt::AlignCenter);

        verticalLayout_27->addWidget(label_21);


        horizontalLayout_16->addLayout(verticalLayout_27);

        verticalLayout_28 = new QVBoxLayout();
        verticalLayout_28->setSpacing(6);
        verticalLayout_28->setObjectName(QStringLiteral("verticalLayout_28"));
        verticalLayout_28->setContentsMargins(-1, -1, 0, -1);
        filEnvelopeRelDial = new QDial(filEnvGroupbox);
        filEnvelopeRelDial->setObjectName(QStringLiteral("filEnvelopeRelDial"));
        filEnvelopeRelDial->setMinimum(1);
        filEnvelopeRelDial->setMaximum(200);

        verticalLayout_28->addWidget(filEnvelopeRelDial);

        label_22 = new QLabel(filEnvGroupbox);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(label_22);


        horizontalLayout_16->addLayout(verticalLayout_28);


        verticalLayout_29->addLayout(horizontalLayout_16);


        mainENVLayout->addWidget(filEnvGroupbox);


        mainContentLayout->addLayout(mainENVLayout);

        line_3 = new QFrame(mainPage);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setStyleSheet(QLatin1String("Line {\n"
"border: 2px solid gray; \n"
"}"));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);

        mainContentLayout->addWidget(line_3);

        mainMonitorLayout = new QVBoxLayout();
        mainMonitorLayout->setSpacing(6);
        mainMonitorLayout->setObjectName(QStringLiteral("mainMonitorLayout"));
        mainMonitorLayout->setContentsMargins(-1, -1, -1, 0);
        monitroGroupbox = new QGroupBox(mainPage);
        monitroGroupbox->setObjectName(QStringLiteral("monitroGroupbox"));
        sizePolicy.setHeightForWidth(monitroGroupbox->sizePolicy().hasHeightForWidth());
        monitroGroupbox->setSizePolicy(sizePolicy);
        monitroGroupbox->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
" } "));
        verticalLayout_11 = new QVBoxLayout(monitroGroupbox);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(11, 11, 11, 11);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        monitorPlot = new QCustomPlot(monitroGroupbox);
        monitorPlot->setObjectName(QStringLiteral("monitorPlot"));

        verticalLayout_11->addWidget(monitorPlot);


        mainMonitorLayout->addWidget(monitroGroupbox);

        groupBox_2 = new QGroupBox(mainPage);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setStyleSheet(QLatin1String("QGroupBox::title { \n"
"    background-color: transparent;\n"
"     subcontrol-position: top left; /* position at the top left*/ \n"
"     padding:2 13px;\n"
" } \n"
"QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"     padding-top:12px;\n"
" } "));
        verticalLayout_38 = new QVBoxLayout(groupBox_2);
        verticalLayout_38->setSpacing(6);
        verticalLayout_38->setContentsMargins(11, 11, 11, 11);
        verticalLayout_38->setObjectName(QStringLiteral("verticalLayout_38"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(-1, 0, -1, -1);
        pushButton_4 = new QPushButton(groupBox_2);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setMinimumSize(QSize(102, 44));
        pushButton_4->setMaximumSize(QSize(102, 44));
        pushButton_4->setStyleSheet(QLatin1String("background:transparent;\n"
"border:none;\n"
"transform: rotate(7deg);"));
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/pictures/pictures/s_off.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon11.addFile(QStringLiteral(":/pictures/pictures/s_on.png"), QSize(), QIcon::Normal, QIcon::On);
        icon11.addFile(QStringLiteral(":/pictures/pictures/s_off.png"), QSize(), QIcon::Disabled, QIcon::Off);
        icon11.addFile(QStringLiteral(":/pictures/pictures/s_on.png"), QSize(), QIcon::Disabled, QIcon::On);
        icon11.addFile(QStringLiteral(":/pictures/pictures/s_off.png"), QSize(), QIcon::Active, QIcon::Off);
        icon11.addFile(QStringLiteral(":/pictures/pictures/s_on.png"), QSize(), QIcon::Active, QIcon::On);
        icon11.addFile(QStringLiteral(":/pictures/pictures/s_off.png"), QSize(), QIcon::Selected, QIcon::Off);
        icon11.addFile(QStringLiteral(":/pictures/pictures/s_on.png"), QSize(), QIcon::Selected, QIcon::On);
        pushButton_4->setIcon(icon11);
        pushButton_4->setIconSize(QSize(102, 44));
        pushButton_4->setCheckable(true);
        pushButton_4->setAutoDefault(true);
        pushButton_4->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_4);


        verticalLayout_38->addLayout(horizontalLayout_7);

        wiwav = new QLabel(groupBox_2);
        wiwav->setObjectName(QStringLiteral("wiwav"));
        wiwav->setMinimumSize(QSize(200, 250));
        wiwav->setMaximumSize(QSize(500, 216));

        verticalLayout_38->addWidget(wiwav);

        label_23 = new QLabel(groupBox_2);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setLayoutDirection(Qt::LeftToRight);
        label_23->setAlignment(Qt::AlignCenter);

        verticalLayout_38->addWidget(label_23);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        verticalLayout_38->addItem(horizontalSpacer_3);


        mainMonitorLayout->addWidget(groupBox_2);


        mainContentLayout->addLayout(mainMonitorLayout);


        mainLayout->addLayout(mainContentLayout);

        mainFooterLayout = new QHBoxLayout();
        mainFooterLayout->setSpacing(6);
        mainFooterLayout->setObjectName(QStringLiteral("mainFooterLayout"));

        mainLayout->addLayout(mainFooterLayout);


        horizontalLayout->addLayout(mainLayout);

        verticalLayout_53 = new QVBoxLayout();
        verticalLayout_53->setSpacing(6);
        verticalLayout_53->setObjectName(QStringLiteral("verticalLayout_53"));

        horizontalLayout->addLayout(verticalLayout_53);

        stackedWidget->addWidget(mainPage);
        algorithmPage = new QWidget();
        algorithmPage->setObjectName(QStringLiteral("algorithmPage"));
        verticalLayout_33 = new QVBoxLayout(algorithmPage);
        verticalLayout_33->setSpacing(6);
        verticalLayout_33->setContentsMargins(11, 11, 11, 11);
        verticalLayout_33->setObjectName(QStringLiteral("verticalLayout_33"));
        algorithmControlLayout = new QHBoxLayout();
        algorithmControlLayout->setSpacing(6);
        algorithmControlLayout->setObjectName(QStringLiteral("algorithmControlLayout"));
        algorithmControlLayout->setSizeConstraint(QLayout::SetMinimumSize);
        settingsTitleLabel_2 = new QLabel(algorithmPage);
        settingsTitleLabel_2->setObjectName(QStringLiteral("settingsTitleLabel_2"));

        algorithmControlLayout->addWidget(settingsTitleLabel_2);

        settingsControlLayoutSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        algorithmControlLayout->addItem(settingsControlLayoutSpacer_2);

        algorithmBackButton = new QPushButton(algorithmPage);
        algorithmBackButton->setObjectName(QStringLiteral("algorithmBackButton"));
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/pictures/pictures/back.png"), QSize(), QIcon::Normal, QIcon::Off);
        algorithmBackButton->setIcon(icon12);

        algorithmControlLayout->addWidget(algorithmBackButton);

        algorithmCloseButton = new QPushButton(algorithmPage);
        algorithmCloseButton->setObjectName(QStringLiteral("algorithmCloseButton"));
        algorithmCloseButton->setIcon(icon3);

        algorithmControlLayout->addWidget(algorithmCloseButton);


        verticalLayout_33->addLayout(algorithmControlLayout);

        tabWidget_4 = new QTabWidget(algorithmPage);
        tabWidget_4->setObjectName(QStringLiteral("tabWidget_4"));
        tab_17 = new QWidget();
        tab_17->setObjectName(QStringLiteral("tab_17"));
        horizontalLayout_50 = new QHBoxLayout(tab_17);
        horizontalLayout_50->setSpacing(6);
        horizontalLayout_50->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_50->setObjectName(QStringLiteral("horizontalLayout_50"));
        groupBox_17 = new QGroupBox(tab_17);
        groupBox_17->setObjectName(QStringLiteral("groupBox_17"));
        groupBox_17->setMaximumSize(QSize(300, 16777215));
        verticalLayout_73 = new QVBoxLayout(groupBox_17);
        verticalLayout_73->setSpacing(6);
        verticalLayout_73->setContentsMargins(11, 11, 11, 11);
        verticalLayout_73->setObjectName(QStringLiteral("verticalLayout_73"));
        label_43 = new QLabel(groupBox_17);
        label_43->setObjectName(QStringLiteral("label_43"));

        verticalLayout_73->addWidget(label_43);

        nameToAdd = new QLineEdit(groupBox_17);
        nameToAdd->setObjectName(QStringLiteral("nameToAdd"));

        verticalLayout_73->addWidget(nameToAdd);

        line_24 = new QFrame(groupBox_17);
        line_24->setObjectName(QStringLiteral("line_24"));
        line_24->setFrameShape(QFrame::HLine);
        line_24->setFrameShadow(QFrame::Sunken);

        verticalLayout_73->addWidget(line_24);

        label_44 = new QLabel(groupBox_17);
        label_44->setObjectName(QStringLiteral("label_44"));

        verticalLayout_73->addWidget(label_44);

        sampFreqSpinBox = new QSpinBox(groupBox_17);
        sampFreqSpinBox->setObjectName(QStringLiteral("sampFreqSpinBox"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(sampFreqSpinBox->sizePolicy().hasHeightForWidth());
        sampFreqSpinBox->setSizePolicy(sizePolicy3);
        sampFreqSpinBox->setMinimum(400);
        sampFreqSpinBox->setMaximum(1000);

        verticalLayout_73->addWidget(sampFreqSpinBox);

        line_26 = new QFrame(groupBox_17);
        line_26->setObjectName(QStringLiteral("line_26"));
        line_26->setFrameShape(QFrame::HLine);
        line_26->setFrameShadow(QFrame::Sunken);

        verticalLayout_73->addWidget(line_26);

        label_45 = new QLabel(groupBox_17);
        label_45->setObjectName(QStringLiteral("label_45"));

        verticalLayout_73->addWidget(label_45);

        ampSpinBox = new QDoubleSpinBox(groupBox_17);
        ampSpinBox->setObjectName(QStringLiteral("ampSpinBox"));
        ampSpinBox->setMaximum(1);
        ampSpinBox->setSingleStep(0.01);
        ampSpinBox->setValue(1);

        verticalLayout_73->addWidget(ampSpinBox);

        line_28 = new QFrame(groupBox_17);
        line_28->setObjectName(QStringLiteral("line_28"));
        line_28->setFrameShape(QFrame::HLine);
        line_28->setFrameShadow(QFrame::Sunken);

        verticalLayout_73->addWidget(line_28);

        label_46 = new QLabel(groupBox_17);
        label_46->setObjectName(QStringLiteral("label_46"));

        verticalLayout_73->addWidget(label_46);

        freqSpinBox = new QDoubleSpinBox(groupBox_17);
        freqSpinBox->setObjectName(QStringLiteral("freqSpinBox"));
        freqSpinBox->setMinimum(1);
        freqSpinBox->setMaximum(200);
        freqSpinBox->setSingleStep(0.01);

        verticalLayout_73->addWidget(freqSpinBox);

        line_30 = new QFrame(groupBox_17);
        line_30->setObjectName(QStringLiteral("line_30"));
        line_30->setFrameShape(QFrame::HLine);
        line_30->setFrameShadow(QFrame::Sunken);

        verticalLayout_73->addWidget(line_30);

        label_8 = new QLabel(groupBox_17);
        label_8->setObjectName(QStringLiteral("label_8"));

        verticalLayout_73->addWidget(label_8);

        timeSpinBox = new QDoubleSpinBox(groupBox_17);
        timeSpinBox->setObjectName(QStringLiteral("timeSpinBox"));
        timeSpinBox->setMinimum(1);
        timeSpinBox->setMaximum(10);
        timeSpinBox->setSingleStep(0.01);

        verticalLayout_73->addWidget(timeSpinBox);

        line_4 = new QFrame(groupBox_17);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);

        verticalLayout_73->addWidget(line_4);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        horizontalLayout_19->setContentsMargins(-1, 0, -1, -1);
        verticalLayout_35 = new QVBoxLayout();
        verticalLayout_35->setSpacing(6);
        verticalLayout_35->setObjectName(QStringLiteral("verticalLayout_35"));
        verticalLayout_35->setContentsMargins(-1, -1, 0, -1);
        label_47 = new QLabel(groupBox_17);
        label_47->setObjectName(QStringLiteral("label_47"));

        verticalLayout_35->addWidget(label_47);

        horizontalLayout_54 = new QHBoxLayout();
        horizontalLayout_54->setSpacing(6);
        horizontalLayout_54->setObjectName(QStringLiteral("horizontalLayout_54"));
        phaseLevel = new QLCDNumber(groupBox_17);
        phaseLevel->setObjectName(QStringLiteral("phaseLevel"));
        phaseLevel->setEnabled(true);
        phaseLevel->setStyleSheet(QStringLiteral(""));
        phaseLevel->setFrameShape(QFrame::NoFrame);

        horizontalLayout_54->addWidget(phaseLevel);

        SigPhaSeterToAdd = new QDial(groupBox_17);
        SigPhaSeterToAdd->setObjectName(QStringLiteral("SigPhaSeterToAdd"));
        SigPhaSeterToAdd->setEnabled(true);
        SigPhaSeterToAdd->setMaximum(360);
        SigPhaSeterToAdd->setPageStep(1);

        horizontalLayout_54->addWidget(SigPhaSeterToAdd);


        verticalLayout_35->addLayout(horizontalLayout_54);


        horizontalLayout_19->addLayout(verticalLayout_35);

        line_6 = new QFrame(groupBox_17);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setFrameShape(QFrame::VLine);
        line_6->setFrameShadow(QFrame::Sunken);

        horizontalLayout_19->addWidget(line_6);

        verticalLayout_75 = new QVBoxLayout();
        verticalLayout_75->setSpacing(6);
        verticalLayout_75->setObjectName(QStringLiteral("verticalLayout_75"));
        label_49 = new QLabel(groupBox_17);
        label_49->setObjectName(QStringLiteral("label_49"));

        verticalLayout_75->addWidget(label_49);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        horizontalLayout_21->setContentsMargins(-1, 0, 0, -1);
        aproxLevel = new QLCDNumber(groupBox_17);
        aproxLevel->setObjectName(QStringLiteral("aproxLevel"));
        aproxLevel->setFrameShape(QFrame::NoFrame);
        aproxLevel->setProperty("intValue", QVariant(1));

        horizontalLayout_21->addWidget(aproxLevel);

        SigAproxLvL = new QDial(groupBox_17);
        SigAproxLvL->setObjectName(QStringLiteral("SigAproxLvL"));
        SigAproxLvL->setMinimum(1);
        SigAproxLvL->setMaximum(20);
        SigAproxLvL->setPageStep(1);

        horizontalLayout_21->addWidget(SigAproxLvL);


        verticalLayout_75->addLayout(horizontalLayout_21);


        horizontalLayout_19->addLayout(verticalLayout_75);


        verticalLayout_73->addLayout(horizontalLayout_19);

        line_32 = new QFrame(groupBox_17);
        line_32->setObjectName(QStringLiteral("line_32"));
        line_32->setFrameShape(QFrame::HLine);
        line_32->setFrameShadow(QFrame::Sunken);

        verticalLayout_73->addWidget(line_32);

        horizontalLayout_55 = new QHBoxLayout();
        horizontalLayout_55->setSpacing(6);
        horizontalLayout_55->setObjectName(QStringLiteral("horizontalLayout_55"));
        label_48 = new QLabel(groupBox_17);
        label_48->setObjectName(QStringLiteral("label_48"));
        label_48->setMaximumSize(QSize(50, 16777215));

        horizontalLayout_55->addWidget(label_48);

        verticalLayout_74 = new QVBoxLayout();
        verticalLayout_74->setSpacing(6);
        verticalLayout_74->setObjectName(QStringLiteral("verticalLayout_74"));
        setSinc = new QRadioButton(groupBox_17);
        setSinc->setObjectName(QStringLiteral("setSinc"));
        setSinc->setChecked(true);

        verticalLayout_74->addWidget(setSinc);

        setSin = new QRadioButton(groupBox_17);
        setSin->setObjectName(QStringLiteral("setSin"));
        setSin->setChecked(false);

        verticalLayout_74->addWidget(setSin);

        setSquare = new QRadioButton(groupBox_17);
        setSquare->setObjectName(QStringLiteral("setSquare"));

        verticalLayout_74->addWidget(setSquare);

        setSqAprox = new QRadioButton(groupBox_17);
        setSqAprox->setObjectName(QStringLiteral("setSqAprox"));

        verticalLayout_74->addWidget(setSqAprox);

        setTri = new QRadioButton(groupBox_17);
        setTri->setObjectName(QStringLiteral("setTri"));

        verticalLayout_74->addWidget(setTri);

        setTriAprox = new QRadioButton(groupBox_17);
        setTriAprox->setObjectName(QStringLiteral("setTriAprox"));

        verticalLayout_74->addWidget(setTriAprox);

        setSaw = new QRadioButton(groupBox_17);
        setSaw->setObjectName(QStringLiteral("setSaw"));

        verticalLayout_74->addWidget(setSaw);

        setSawAprox = new QRadioButton(groupBox_17);
        setSawAprox->setObjectName(QStringLiteral("setSawAprox"));

        verticalLayout_74->addWidget(setSawAprox);


        horizontalLayout_55->addLayout(verticalLayout_74);

        line_5 = new QFrame(groupBox_17);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::VLine);
        line_5->setFrameShadow(QFrame::Sunken);

        horizontalLayout_55->addWidget(line_5);

        verticalLayout_37 = new QVBoxLayout();
        verticalLayout_37->setSpacing(6);
        verticalLayout_37->setObjectName(QStringLiteral("verticalLayout_37"));
        verticalLayout_37->setSizeConstraint(QLayout::SetMaximumSize);
        verticalLayout_37->setContentsMargins(-1, 0, -1, -1);
        label_10 = new QLabel(groupBox_17);
        label_10->setObjectName(QStringLiteral("label_10"));

        verticalLayout_37->addWidget(label_10);

        radioButton_7 = new QRadioButton(groupBox_17);
        radioButton_7->setObjectName(QStringLiteral("radioButton_7"));

        verticalLayout_37->addWidget(radioButton_7);

        radioButton_9 = new QRadioButton(groupBox_17);
        radioButton_9->setObjectName(QStringLiteral("radioButton_9"));

        verticalLayout_37->addWidget(radioButton_9);


        horizontalLayout_55->addLayout(verticalLayout_37);


        verticalLayout_73->addLayout(horizontalLayout_55);

        addSignalButton = new QPushButton(groupBox_17);
        addSignalButton->setObjectName(QStringLiteral("addSignalButton"));

        verticalLayout_73->addWidget(addSignalButton);


        horizontalLayout_50->addWidget(groupBox_17);

        line_33 = new QFrame(tab_17);
        line_33->setObjectName(QStringLiteral("line_33"));
        line_33->setFrameShape(QFrame::VLine);
        line_33->setFrameShadow(QFrame::Sunken);

        horizontalLayout_50->addWidget(line_33);

        verticalLayout_76 = new QVBoxLayout();
        verticalLayout_76->setSpacing(6);
        verticalLayout_76->setObjectName(QStringLiteral("verticalLayout_76"));
        verticalLayout_76->setContentsMargins(0, -1, -1, 0);
        tableView = new QTableView(tab_17);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setMinimumSize(QSize(0, 0));
        tableView->setMaximumSize(QSize(2800, 1600));
        tableView->setBaseSize(QSize(2, 0));

        verticalLayout_76->addWidget(tableView);

        sigPlot = new QCustomPlot(tab_17);
        sigPlot->setObjectName(QStringLiteral("sigPlot"));
        sizePolicy.setHeightForWidth(sigPlot->sizePolicy().hasHeightForWidth());
        sigPlot->setSizePolicy(sizePolicy);
        sigPlot->setMinimumSize(QSize(0, 0));

        verticalLayout_76->addWidget(sigPlot);

        horizontalLayout_56 = new QHBoxLayout();
        horizontalLayout_56->setSpacing(6);
        horizontalLayout_56->setObjectName(QStringLiteral("horizontalLayout_56"));
        horizontalLayout_56->setContentsMargins(0, 0, -1, -1);
        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_56->addItem(horizontalSpacer_16);

        deleteSelected_3 = new QPushButton(tab_17);
        deleteSelected_3->setObjectName(QStringLiteral("deleteSelected_3"));
        deleteSelected_3->setEnabled(true);

        horizontalLayout_56->addWidget(deleteSelected_3);


        verticalLayout_76->addLayout(horizontalLayout_56);


        horizontalLayout_50->addLayout(verticalLayout_76);

        tabWidget_4->addTab(tab_17, QString());
        tab_19 = new QWidget();
        tab_19->setObjectName(QStringLiteral("tab_19"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(tab_19->sizePolicy().hasHeightForWidth());
        tab_19->setSizePolicy(sizePolicy4);
        tab_19->setMinimumSize(QSize(1320, 0));
        horizontalLayout_58 = new QHBoxLayout(tab_19);
        horizontalLayout_58->setSpacing(6);
        horizontalLayout_58->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_58->setObjectName(QStringLiteral("horizontalLayout_58"));
        groupBox_19 = new QGroupBox(tab_19);
        groupBox_19->setObjectName(QStringLiteral("groupBox_19"));
        groupBox_19->setMaximumSize(QSize(300, 16777215));
        verticalLayout_79 = new QVBoxLayout(groupBox_19);
        verticalLayout_79->setSpacing(6);
        verticalLayout_79->setContentsMargins(11, 11, 11, 11);
        verticalLayout_79->setObjectName(QStringLiteral("verticalLayout_79"));
        listSpectralAnalisis = new QListWidget(groupBox_19);
        listSpectralAnalisis->setObjectName(QStringLiteral("listSpectralAnalisis"));

        verticalLayout_79->addWidget(listSpectralAnalisis);

        catchIFFTButton = new QPushButton(groupBox_19);
        catchIFFTButton->setObjectName(QStringLiteral("catchIFFTButton"));

        verticalLayout_79->addWidget(catchIFFTButton);

        catchFFTButton = new QPushButton(groupBox_19);
        catchFFTButton->setObjectName(QStringLiteral("catchFFTButton"));

        verticalLayout_79->addWidget(catchFFTButton);


        horizontalLayout_58->addWidget(groupBox_19);

        verticalLayout_36 = new QVBoxLayout();
        verticalLayout_36->setSpacing(6);
        verticalLayout_36->setObjectName(QStringLiteral("verticalLayout_36"));
        verticalLayout_36->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_36->setContentsMargins(-1, -1, 0, 0);
        frame_16 = new QFrame(tab_19);
        frame_16->setObjectName(QStringLiteral("frame_16"));
        QSizePolicy sizePolicy5(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy5.setHorizontalStretch(100);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(frame_16->sizePolicy().hasHeightForWidth());
        frame_16->setSizePolicy(sizePolicy5);
        frame_16->setMinimumSize(QSize(100, 0));
        frame_16->setFrameShape(QFrame::StyledPanel);
        frame_16->setFrameShadow(QFrame::Raised);
        verticalLayout_80 = new QVBoxLayout(frame_16);
        verticalLayout_80->setSpacing(0);
        verticalLayout_80->setContentsMargins(11, 11, 11, 11);
        verticalLayout_80->setObjectName(QStringLiteral("verticalLayout_80"));
        verticalLayout_80->setContentsMargins(0, 0, 0, 0);
        fftPlot = new QCustomPlot(frame_16);
        fftPlot->setObjectName(QStringLiteral("fftPlot"));
        sizePolicy2.setHeightForWidth(fftPlot->sizePolicy().hasHeightForWidth());
        fftPlot->setSizePolicy(sizePolicy2);
        fftPlot->setMinimumSize(QSize(0, 0));

        verticalLayout_80->addWidget(fftPlot);


        verticalLayout_36->addWidget(frame_16);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QStringLiteral("horizontalLayout_24"));
        horizontalLayout_24->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_24->addItem(horizontalSpacer_8);

        catchSpectral = new QPushButton(tab_19);
        catchSpectral->setObjectName(QStringLiteral("catchSpectral"));

        horizontalLayout_24->addWidget(catchSpectral);


        verticalLayout_36->addLayout(horizontalLayout_24);


        horizontalLayout_58->addLayout(verticalLayout_36);

        tabWidget_4->addTab(tab_19, QString());
        tab_20 = new QWidget();
        tab_20->setObjectName(QStringLiteral("tab_20"));
        horizontalLayout_17 = new QHBoxLayout(tab_20);
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        groupBox_21 = new QGroupBox(tab_20);
        groupBox_21->setObjectName(QStringLiteral("groupBox_21"));
        sizePolicy2.setHeightForWidth(groupBox_21->sizePolicy().hasHeightForWidth());
        groupBox_21->setSizePolicy(sizePolicy2);
        groupBox_21->setMaximumSize(QSize(300, 16777215));
        verticalLayout_34 = new QVBoxLayout(groupBox_21);
        verticalLayout_34->setSpacing(6);
        verticalLayout_34->setContentsMargins(11, 11, 11, 11);
        verticalLayout_34->setObjectName(QStringLiteral("verticalLayout_34"));
        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        horizontalLayout_18->setContentsMargins(-1, -1, 0, -1);
        listCorelationX = new QListWidget(groupBox_21);
        listCorelationX->setObjectName(QStringLiteral("listCorelationX"));
        sizePolicy4.setHeightForWidth(listCorelationX->sizePolicy().hasHeightForWidth());
        listCorelationX->setSizePolicy(sizePolicy4);

        horizontalLayout_18->addWidget(listCorelationX);

        listCorelationY = new QListWidget(groupBox_21);
        listCorelationY->setObjectName(QStringLiteral("listCorelationY"));
        sizePolicy4.setHeightForWidth(listCorelationY->sizePolicy().hasHeightForWidth());
        listCorelationY->setSizePolicy(sizePolicy4);

        horizontalLayout_18->addWidget(listCorelationY);


        verticalLayout_34->addLayout(horizontalLayout_18);

        rxyCheck = new QPushButton(groupBox_21);
        rxyCheck->setObjectName(QStringLiteral("rxyCheck"));

        verticalLayout_34->addWidget(rxyCheck);


        horizontalLayout_17->addWidget(groupBox_21);

        frame_18 = new QFrame(tab_20);
        frame_18->setObjectName(QStringLiteral("frame_18"));
        frame_18->setFrameShape(QFrame::StyledPanel);
        frame_18->setFrameShadow(QFrame::Raised);
        verticalLayout_84 = new QVBoxLayout(frame_18);
        verticalLayout_84->setSpacing(0);
        verticalLayout_84->setContentsMargins(11, 11, 11, 11);
        verticalLayout_84->setObjectName(QStringLiteral("verticalLayout_84"));
        verticalLayout_84->setContentsMargins(0, 0, 0, 0);
        rxyPlot = new QCustomPlot(frame_18);
        rxyPlot->setObjectName(QStringLiteral("rxyPlot"));
        sizePolicy2.setHeightForWidth(rxyPlot->sizePolicy().hasHeightForWidth());
        rxyPlot->setSizePolicy(sizePolicy2);
        rxyPlot->setMinimumSize(QSize(100, 100));

        verticalLayout_84->addWidget(rxyPlot);


        horizontalLayout_17->addWidget(frame_18);

        tabWidget_4->addTab(tab_20, QString());
        tab_21 = new QWidget();
        tab_21->setObjectName(QStringLiteral("tab_21"));
        horizontalLayout_62 = new QHBoxLayout(tab_21);
        horizontalLayout_62->setSpacing(6);
        horizontalLayout_62->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_62->setObjectName(QStringLiteral("horizontalLayout_62"));
        groupBox_22 = new QGroupBox(tab_21);
        groupBox_22->setObjectName(QStringLiteral("groupBox_22"));
        groupBox_22->setMaximumSize(QSize(300, 16777215));
        verticalLayout_85 = new QVBoxLayout(groupBox_22);
        verticalLayout_85->setSpacing(6);
        verticalLayout_85->setContentsMargins(11, 11, 11, 11);
        verticalLayout_85->setObjectName(QStringLiteral("verticalLayout_85"));
        listOperationsA = new QListWidget(groupBox_22);
        listOperationsA->setObjectName(QStringLiteral("listOperationsA"));

        verticalLayout_85->addWidget(listOperationsA);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        horizontalLayout_20->setContentsMargins(-1, 0, -1, -1);
        additionSignal = new QPushButton(groupBox_22);
        additionSignal->setObjectName(QStringLiteral("additionSignal"));
        additionSignal->setMinimumSize(QSize(0, 0));
        additionSignal->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_20->addWidget(additionSignal);

        subtractionSignal = new QPushButton(groupBox_22);
        subtractionSignal->setObjectName(QStringLiteral("subtractionSignal"));
        sizePolicy3.setHeightForWidth(subtractionSignal->sizePolicy().hasHeightForWidth());
        subtractionSignal->setSizePolicy(sizePolicy3);
        subtractionSignal->setMinimumSize(QSize(0, 0));
        subtractionSignal->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_20->addWidget(subtractionSignal);

        multiplicationSignal = new QPushButton(groupBox_22);
        multiplicationSignal->setObjectName(QStringLiteral("multiplicationSignal"));
        multiplicationSignal->setMinimumSize(QSize(0, 0));
        multiplicationSignal->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_20->addWidget(multiplicationSignal);

        divisionSignal = new QPushButton(groupBox_22);
        divisionSignal->setObjectName(QStringLiteral("divisionSignal"));
        divisionSignal->setMinimumSize(QSize(0, 0));
        divisionSignal->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_20->addWidget(divisionSignal);


        verticalLayout_85->addLayout(horizontalLayout_20);

        listOperationsB = new QListWidget(groupBox_22);
        listOperationsB->setObjectName(QStringLiteral("listOperationsB"));

        verticalLayout_85->addWidget(listOperationsB);


        horizontalLayout_62->addWidget(groupBox_22);

        frame_19 = new QFrame(tab_21);
        frame_19->setObjectName(QStringLiteral("frame_19"));
        frame_19->setFrameShape(QFrame::StyledPanel);
        frame_19->setFrameShadow(QFrame::Raised);
        horizontalLayout_63 = new QHBoxLayout(frame_19);
        horizontalLayout_63->setSpacing(0);
        horizontalLayout_63->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_63->setObjectName(QStringLiteral("horizontalLayout_63"));
        horizontalLayout_63->setContentsMargins(0, 0, 0, 0);
        opPlot = new QCustomPlot(frame_19);
        opPlot->setObjectName(QStringLiteral("opPlot"));

        horizontalLayout_63->addWidget(opPlot);


        horizontalLayout_62->addWidget(frame_19);

        tabWidget_4->addTab(tab_21, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        horizontalLayout_2 = new QHBoxLayout(tab);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        groupBox_24 = new QGroupBox(tab);
        groupBox_24->setObjectName(QStringLiteral("groupBox_24"));
        groupBox_24->setMaximumSize(QSize(300, 16777215));
        verticalLayout_90 = new QVBoxLayout(groupBox_24);
        verticalLayout_90->setSpacing(6);
        verticalLayout_90->setContentsMargins(11, 11, 11, 11);
        verticalLayout_90->setObjectName(QStringLiteral("verticalLayout_90"));
        listWindowing = new QListWidget(groupBox_24);
        listWindowing->setObjectName(QStringLiteral("listWindowing"));

        verticalLayout_90->addWidget(listWindowing);

        windowingHamming = new QPushButton(groupBox_24);
        windowingHamming->setObjectName(QStringLiteral("windowingHamming"));

        verticalLayout_90->addWidget(windowingHamming);

        windowingHanning = new QPushButton(groupBox_24);
        windowingHanning->setObjectName(QStringLiteral("windowingHanning"));

        verticalLayout_90->addWidget(windowingHanning);

        windowingBlackman = new QPushButton(groupBox_24);
        windowingBlackman->setObjectName(QStringLiteral("windowingBlackman"));

        verticalLayout_90->addWidget(windowingBlackman);

        windowingBlackmanHarris = new QPushButton(groupBox_24);
        windowingBlackmanHarris->setObjectName(QStringLiteral("windowingBlackmanHarris"));

        verticalLayout_90->addWidget(windowingBlackmanHarris);

        windowingBarlettHanning = new QPushButton(groupBox_24);
        windowingBarlettHanning->setObjectName(QStringLiteral("windowingBarlettHanning"));

        verticalLayout_90->addWidget(windowingBarlettHanning);


        horizontalLayout_2->addWidget(groupBox_24);

        frame = new QFrame(tab);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_39 = new QVBoxLayout(frame);
        verticalLayout_39->setSpacing(6);
        verticalLayout_39->setContentsMargins(11, 11, 11, 11);
        verticalLayout_39->setObjectName(QStringLiteral("verticalLayout_39"));
        verticalLayout_39->setContentsMargins(0, 0, 0, 0);
        winPlot = new QCustomPlot(frame);
        winPlot->setObjectName(QStringLiteral("winPlot"));

        verticalLayout_39->addWidget(winPlot);


        horizontalLayout_2->addWidget(frame);

        tabWidget_4->addTab(tab, QString());
        tab_22 = new QWidget();
        tab_22->setObjectName(QStringLiteral("tab_22"));
        horizontalLayout_64 = new QHBoxLayout(tab_22);
        horizontalLayout_64->setSpacing(6);
        horizontalLayout_64->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_64->setObjectName(QStringLiteral("horizontalLayout_64"));
        groupBox_23 = new QGroupBox(tab_22);
        groupBox_23->setObjectName(QStringLiteral("groupBox_23"));
        groupBox_23->setMaximumSize(QSize(300, 16777215));
        verticalLayout_88 = new QVBoxLayout(groupBox_23);
        verticalLayout_88->setSpacing(6);
        verticalLayout_88->setContentsMargins(11, 11, 11, 11);
        verticalLayout_88->setObjectName(QStringLiteral("verticalLayout_88"));

        horizontalLayout_64->addWidget(groupBox_23);

        frame_20 = new QFrame(tab_22);
        frame_20->setObjectName(QStringLiteral("frame_20"));
        frame_20->setFrameShape(QFrame::StyledPanel);
        frame_20->setFrameShadow(QFrame::Raised);
        verticalLayout_89 = new QVBoxLayout(frame_20);
        verticalLayout_89->setSpacing(0);
        verticalLayout_89->setContentsMargins(11, 11, 11, 11);
        verticalLayout_89->setObjectName(QStringLiteral("verticalLayout_89"));
        verticalLayout_89->setContentsMargins(0, 0, 0, 0);
        otherPlot = new QCustomPlot(frame_20);
        otherPlot->setObjectName(QStringLiteral("otherPlot"));
        otherPlot->setMinimumSize(QSize(100, 100));

        verticalLayout_89->addWidget(otherPlot);


        horizontalLayout_64->addWidget(frame_20);

        tabWidget_4->addTab(tab_22, QString());

        verticalLayout_33->addWidget(tabWidget_4);

        stackedWidget->addWidget(algorithmPage);
        settingsPage = new QWidget();
        settingsPage->setObjectName(QStringLiteral("settingsPage"));
        verticalLayout_52 = new QVBoxLayout(settingsPage);
        verticalLayout_52->setSpacing(6);
        verticalLayout_52->setContentsMargins(11, 11, 11, 11);
        verticalLayout_52->setObjectName(QStringLiteral("verticalLayout_52"));
        settingsControlLayout = new QHBoxLayout();
        settingsControlLayout->setSpacing(6);
        settingsControlLayout->setObjectName(QStringLiteral("settingsControlLayout"));
        settingsControlLayout->setSizeConstraint(QLayout::SetMinimumSize);
        settingsTitleLabel = new QLabel(settingsPage);
        settingsTitleLabel->setObjectName(QStringLiteral("settingsTitleLabel"));

        settingsControlLayout->addWidget(settingsTitleLabel);

        settingsControlLayoutSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        settingsControlLayout->addItem(settingsControlLayoutSpacer);

        settingsBackButton = new QPushButton(settingsPage);
        settingsBackButton->setObjectName(QStringLiteral("settingsBackButton"));
        settingsBackButton->setIcon(icon12);

        settingsControlLayout->addWidget(settingsBackButton);

        settingsCloseButton = new QPushButton(settingsPage);
        settingsCloseButton->setObjectName(QStringLiteral("settingsCloseButton"));
        settingsCloseButton->setIcon(icon3);

        settingsControlLayout->addWidget(settingsCloseButton);


        verticalLayout_52->addLayout(settingsControlLayout);

        settingsFooterLayout = new QHBoxLayout();
        settingsFooterLayout->setSpacing(6);
        settingsFooterLayout->setObjectName(QStringLiteral("settingsFooterLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        settingsFooterLayout->addItem(verticalSpacer_2);


        verticalLayout_52->addLayout(settingsFooterLayout);

        stackedWidget->addWidget(settingsPage);

        verticalLayout->addWidget(stackedWidget);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);
        pushButton_4->setDefault(true);
        tabWidget_4->setCurrentIndex(5);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "aPL - Audio Processing Library", 0));
        mainTitleLabel->setText(QApplication::translate("MainWindow", "aPL - Audio Processing Library", 0));
        mainBrainButton->setText(QString());
        mainSettingsButton->setText(QString());
        mainCloseButton->setText(QString());
        lfo_1GroupBox->setTitle(QApplication::translate("MainWindow", "LFO 1", 0));
        label_13->setText(QApplication::translate("MainWindow", "AMOUNT", 0));
        label_14->setText(QApplication::translate("MainWindow", "RATE", 0));
        lfo_1ShapeSaw->setText(QString());
        lfo_1ShapeSquare->setText(QString());
        lfo_1ShapeSin->setText(QString());
        lfo_1ShapeNoise->setText(QString());
        lfo_2GroupBox->setTitle(QApplication::translate("MainWindow", "LFO 2", 0));
        label_11->setText(QApplication::translate("MainWindow", "AMOUNT", 0));
        label_12->setText(QApplication::translate("MainWindow", "RATE", 0));
        lfo_2ShapeSaw->setText(QString());
        lfo_2ShapeSquare->setText(QString());
        lfo_2ShapeSin->setText(QString());
        lfo_2ShapeNoise->setText(QString());
        lfo_1DestGroupBox->setTitle(QApplication::translate("MainWindow", "LFO 1 DEST", 0));
        radioButton_5->setText(QApplication::translate("MainWindow", "OSC 1", 0));
        radioButton_6->setText(QApplication::translate("MainWindow", "OSC 1+2", 0));
        lfoMixGroupBox->setTitle(QApplication::translate("MainWindow", "LFO's MIX", 0));
        checkBox->setText(QApplication::translate("MainWindow", "MIX LFO's", 0));
        radioButton->setText(QApplication::translate("MainWindow", "OSC 1", 0));
        radioButton_2->setText(QApplication::translate("MainWindow", "OSC 2", 0));
        radioButton_3->setText(QApplication::translate("MainWindow", "OSC 1+2", 0));
        radioButton_4->setText(QApplication::translate("MainWindow", "OSC MIX", 0));
        lfo_2DestGroupBox->setTitle(QApplication::translate("MainWindow", "LFO 2 DEST", 0));
        radioButton_8->setText(QApplication::translate("MainWindow", "OSC 2", 0));
        radioButton_10->setText(QApplication::translate("MainWindow", "OSC MIX", 0));
        label_7->setText(QApplication::translate("MainWindow", "LFO's\n"
"MIX", 0));
        filtherGroupBox->setTitle(QApplication::translate("MainWindow", "FILTHER", 0));
        label_2->setText(QApplication::translate("MainWindow", "RESO", 0));
        label->setText(QApplication::translate("MainWindow", "CUTOFF", 0));
        pushButton_3->setText(QString());
        pushButton_2->setText(QString());
        pushButton->setText(QString());
        groupBox->setTitle(QApplication::translate("MainWindow", "Keyboard", 0));
        keyC->setText(QApplication::translate("MainWindow", "C", 0));
        keyC->setShortcut(QApplication::translate("MainWindow", "Z", 0));
        keyD->setText(QApplication::translate("MainWindow", "D", 0));
        keyD->setShortcut(QApplication::translate("MainWindow", "X", 0));
        keyE->setText(QApplication::translate("MainWindow", "E", 0));
        keyE->setShortcut(QApplication::translate("MainWindow", "C", 0));
        keyF->setText(QApplication::translate("MainWindow", "F", 0));
        keyF->setShortcut(QApplication::translate("MainWindow", "V", 0));
        keyG->setText(QApplication::translate("MainWindow", "G", 0));
        keyG->setShortcut(QApplication::translate("MainWindow", "B", 0));
        keyA->setText(QApplication::translate("MainWindow", "A", 0));
        keyA->setShortcut(QApplication::translate("MainWindow", "N", 0));
        keyH->setText(QApplication::translate("MainWindow", "H", 0));
        keyH->setShortcut(QApplication::translate("MainWindow", "M", 0));
        osc_1GroupBox->setTitle(QApplication::translate("MainWindow", "OSC 1", 0));
        label_3->setText(QApplication::translate("MainWindow", "COARSE", 0));
        label_4->setText(QApplication::translate("MainWindow", "FINE", 0));
        osc1Saw->setText(QString());
        osc1Squa->setText(QString());
        osc1Sin->setText(QString());
        osc1Nois->setText(QString());
        osc_2GroupBox->setTitle(QApplication::translate("MainWindow", "OSC 2", 0));
        label_5->setText(QApplication::translate("MainWindow", "COARSE", 0));
        label_6->setText(QApplication::translate("MainWindow", "FINE", 0));
        osc2Saw->setText(QString());
        osc2Squa->setText(QString());
        osc2Sin->setText(QString());
        osc2Nois->setText(QString());
        label_9->setText(QApplication::translate("MainWindow", "OSC's\n"
"MIX", 0));
        ampEnvGroupBox->setTitle(QApplication::translate("MainWindow", "AMP ENV", 0));
        label_15->setText(QApplication::translate("MainWindow", "ATK", 0));
        label_16->setText(QApplication::translate("MainWindow", "DEC", 0));
        label_17->setText(QApplication::translate("MainWindow", "SUS", 0));
        label_18->setText(QApplication::translate("MainWindow", "REL", 0));
        filEnvGroupbox->setTitle(QApplication::translate("MainWindow", "FIL ENV", 0));
        label_19->setText(QApplication::translate("MainWindow", "ATK", 0));
        label_20->setText(QApplication::translate("MainWindow", "DEC", 0));
        label_21->setText(QApplication::translate("MainWindow", "SUS", 0));
        label_22->setText(QApplication::translate("MainWindow", "REL", 0));
        monitroGroupbox->setTitle(QApplication::translate("MainWindow", "MONITOR", 0));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "POWER", 0));
        pushButton_4->setText(QString());
        wiwav->setText(QString());
        label_23->setText(QApplication::translate("MainWindow", "Project by\n"
"SKN WiWav Team\n"
"Lukasz Cierocki\n"
"Cezary Wernik\n"
"Directed by:\n"
"dr in\305\274. Miros\305\202aw \305\201azoryszczak\n"
" dr in\305\274. Tomasz M\304\205ka", 0));
        settingsTitleLabel_2->setText(QApplication::translate("MainWindow", "aPL - Audio Processing Library - Analisis", 0));
        algorithmBackButton->setText(QString());
        algorithmCloseButton->setText(QString());
        groupBox_17->setTitle(QApplication::translate("MainWindow", "Add signal", 0));
        label_43->setText(QApplication::translate("MainWindow", "Name", 0));
        label_44->setText(QApplication::translate("MainWindow", "Sampling frequency (400-1000)[Hz]", 0));
        label_45->setText(QApplication::translate("MainWindow", "Amplitude (0-100)[% of 1]", 0));
        label_46->setText(QApplication::translate("MainWindow", "Frequency (1-200)[Hz]", 0));
        label_8->setText(QApplication::translate("MainWindow", "Time (1 - 10) [s]", 0));
        label_47->setText(QApplication::translate("MainWindow", "Phase (0-360)", 0));
        label_49->setText(QApplication::translate("MainWindow", "Aprox level", 0));
        label_48->setText(QApplication::translate("MainWindow", "Shape:", 0));
        setSinc->setText(QApplication::translate("MainWindow", "sinc", 0));
        setSin->setText(QApplication::translate("MainWindow", "Sinusoidal", 0));
        setSquare->setText(QApplication::translate("MainWindow", "Square ideal", 0));
        setSqAprox->setText(QApplication::translate("MainWindow", "Square aprox", 0));
        setTri->setText(QApplication::translate("MainWindow", "Triangle ideal", 0));
        setTriAprox->setText(QApplication::translate("MainWindow", "Triangle aprox", 0));
        setSaw->setText(QApplication::translate("MainWindow", "Saw ideal", 0));
        setSawAprox->setText(QApplication::translate("MainWindow", "Saw aprox", 0));
        label_10->setText(QApplication::translate("MainWindow", "Depression:", 0));
        radioButton_7->setText(QApplication::translate("MainWindow", "UNIPOLAR", 0));
        radioButton_9->setText(QApplication::translate("MainWindow", "BIPOLAR", 0));
        addSignalButton->setText(QApplication::translate("MainWindow", "Add", 0));
        deleteSelected_3->setText(QApplication::translate("MainWindow", "Delete selected", 0));
        tabWidget_4->setTabText(tabWidget_4->indexOf(tab_17), QApplication::translate("MainWindow", "Manager", 0));
        groupBox_19->setTitle(QApplication::translate("MainWindow", "Select signal to do FFT / IFFT", 0));
        catchIFFTButton->setText(QApplication::translate("MainWindow", "IFFT graph", 0));
        catchFFTButton->setText(QApplication::translate("MainWindow", "FFT graph", 0));
        catchSpectral->setText(QApplication::translate("MainWindow", "Catch FFT(x) / IFFX(X)", 0));
        tabWidget_4->setTabText(tabWidget_4->indexOf(tab_19), QApplication::translate("MainWindow", "Spectral Analisis", 0));
        groupBox_21->setTitle(QApplication::translate("MainWindow", "Select signal", 0));
        rxyCheck->setText(QApplication::translate("MainWindow", "Check", 0));
        tabWidget_4->setTabText(tabWidget_4->indexOf(tab_20), QApplication::translate("MainWindow", "Corelation / Autocorelation", 0));
        groupBox_22->setTitle(QApplication::translate("MainWindow", "Select signal", 0));
        additionSignal->setText(QApplication::translate("MainWindow", "+", 0));
        subtractionSignal->setText(QApplication::translate("MainWindow", "-", 0));
        multiplicationSignal->setText(QApplication::translate("MainWindow", "*", 0));
        divisionSignal->setText(QApplication::translate("MainWindow", "/", 0));
        tabWidget_4->setTabText(tabWidget_4->indexOf(tab_21), QApplication::translate("MainWindow", "Operations", 0));
        groupBox_24->setTitle(QApplication::translate("MainWindow", "Select signal", 0));
        windowingHamming->setText(QApplication::translate("MainWindow", "Hamming", 0));
        windowingHanning->setText(QApplication::translate("MainWindow", "Hanning", 0));
        windowingBlackman->setText(QApplication::translate("MainWindow", "Blackman", 0));
        windowingBlackmanHarris->setText(QApplication::translate("MainWindow", "Blackman - Harris", 0));
        windowingBarlettHanning->setText(QApplication::translate("MainWindow", "Barlett - Hanning", 0));
        tabWidget_4->setTabText(tabWidget_4->indexOf(tab), QApplication::translate("MainWindow", "Windowing", 0));
        groupBox_23->setTitle(QApplication::translate("MainWindow", "Filter setting", 0));
        tabWidget_4->setTabText(tabWidget_4->indexOf(tab_22), QApplication::translate("MainWindow", "Other", 0));
        settingsTitleLabel->setText(QApplication::translate("MainWindow", "aPL - Audio Processing Library - Settings", 0));
        settingsBackButton->setText(QString());
        settingsCloseButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GUIAPL_H
