#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "aPL.h"
#include "QCustomPlot.h"
#include "settings.h"
#include "waveform.h"
#include "worker.h"
//using namespace aPL_nspc;

namespace Ui {
class MainWindow;
}

class GUIaPL : public QMainWindow
{
    Q_OBJECT

public:
    explicit GUIaPL(QWidget *parent = 0);
    ~GUIaPL();

private slots:

    void restoreControlsValues();
    void on_mainSettingsButton_clicked();
    void on_mainCloseButton_clicked();
    void on_settingsCloseButton_clicked();
    void on_settingsBackButton_clicked();
    void on_filEnvelopeAtkDial_valueChanged(int value);
    void on_filEnvelopeDecDial_valueChanged(int value);
    void on_filEnvelopeSusDial_valueChanged(int value);
    void on_filEnvelopeRelDial_valueChanged(int value);
    void on_ampEnvelopeAtkDial_valueChanged(int value);
    void on_ampEnvelopeDecDial_valueChanged(int value);
    void on_ampEnvelopeSusDial_valueChanged(int value);
    void on_ampEnvelopeRelDial_valueChanged(int value);
    void on_lfo1AmountDial_valueChanged(int value);
    void on_lfo1RateDial_valueChanged(int value);
    void on_lfo2AmountDial_valueChanged(int value);
    void on_lfo2RateDial_valueChanged(int value);
    void on_filResoDial_valueChanged(int value);
    void on_filCutOffDial_valueChanged(int value);
    void on_osc1CoarseDial_valueChanged(int value);
    void on_osc1FineDial_valueChanged(int value);
    void on_osc2CoarseDial_valueChanged(int value);
    void on_osc2FineDial_valueChanged(int value);
    void on_lfo_1ShapeSaw_clicked();
    void on_lfo_1ShapeSquare_clicked();
    void on_lfo_1ShapeSin_clicked();
    void on_lfo_1ShapeNoise_clicked();
    void on_lfo_2ShapeSaw_clicked();
    void on_lfo_2ShapeSquare_clicked();
    void on_lfo_2ShapeSin_clicked();
    void on_lfo_2ShapeNoise_clicked();
    void on_mainBrainButton_clicked();
    void on_algorithmCloseButton_clicked();
    void on_algorithmBackButton_clicked();
    void on_addSignalButton_clicked();
    void on_tableView_clicked(const QModelIndex &index);
    void on_SigAproxLvL_valueChanged(int value);
    void on_SigPhaSeterToAdd_valueChanged(int value);
    void on_deleteSelected_3_clicked();
    void on_catchFFTButton_clicked();
    void on_catchIFFTButton_clicked();
    void on_listSpectralAnalisis_clicked(const QModelIndex &index);
    void on_catchSpectral_clicked();
    void on_listCorelationX_clicked(const QModelIndex &index);
    void on_listCorelationY_clicked(const QModelIndex &index);
    void on_rxyCheck_clicked();
    void on_listOperationsA_clicked(const QModelIndex &index);
    void on_listOperationsB_clicked(const QModelIndex &index);
    void on_additionSignal_clicked();
    void on_subtractionSignal_clicked();
    void on_multiplicationSignal_clicked();
    void on_divisionSignal_clicked();

    void on_listWindowing_clicked(const QModelIndex &index);

    void on_windowingHamming_clicked();

    void on_windowingHanning_clicked();

    void on_windowingBlackman_clicked();

    void on_windowingBlackmanHarris_clicked();

    void on_windowingBarlettHanning_clicked();

    void on_osc1Saw_clicked();

    void on_osc1Squa_clicked();

    void on_osc1Sin_clicked();

    void on_osc1Nois_clicked();

    void on_osc2Saw_clicked();

    void on_osc2Squa_clicked();

    void on_osc2Sin_clicked();

    void on_osc2Nois_clicked();
    void on_keyC_clicked();

    void on_keyD_clicked();

    void on_keyE_clicked();

    void on_keyF_clicked();

    void on_keyG_clicked();

    void on_keyA_clicked();

    void on_keyH_clicked();

    void on_oscSlider_valueChanged(int value);

private:

    void initPlots();
    void initTableView();

    void initAmpEnvelopePlot();
    QVector<double> ampEnvelopePlotValue(int atk, int dec, int sus, int rel);
    void repaintAmpEnvelopePlot();

    void repaintFilterEnvelopePlot();   
    QVector<double> generateEnvelopePlotValue(int atk, int dec, int sus, int rel);
    void initFilterEnvelopePlot();

    void initSigPlot();

    void refreshMonitor();

    int getDialValue(QDial *dial);

    void lfoUpdate(QString LFO_name);
    void oscUpdate(QString OSC_name);
    void lfoRestoreShapeButtonCheched(QString LFO_name);
    void oscRestoreShapeButtonCheched(QString OSC_name);
    //signal operations
    void performArithmeticOperation(int typeOperations);
    void performWindowingOperation(int typeWindow);

private:
    Ui::MainWindow *ui;
    aPL engine;

    QStandardItemModel *model;

    QThread thread1;
    QTimer timer1;
    Worker worker1;

    //Pola dla częśći analitycznej
    QVector <waveform> signalList;
    QCPPlotTitle *titleSigPlot;

    //FFT/IFFT
    waveform tmpSpectralSignal;
    int signalIndexToSpectralAnalisis;
    int lastSpectralOperation;
    QCPPlotTitle *titleFftPlot;

    //Rxx/Rxy
    waveform tmpRxxRxySignal;
    int signalIndexXToRxxRxy;
    int signalIndexYToRxxRxy;
    QCPPlotTitle *titleRxxRxyPlot;

    //operations
    waveform tmpOperationSignal;
    int signalIndexOperationsA;
    int signalIndexOperationsB;
    int lastOperatnion;
    QCPPlotTitle *titleOpPlot;

    //windowing
    struct windowingCase
    {
        QCPPlotTitle *title;
        waveform wForm;
        int lastAction;
        int signalIndex;
    } _windowing;


    QString configPath = QString("conf.json");
    settings conf;
};

#endif // MAINWINDOW_H
