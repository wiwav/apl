#include "GUIaPL.h"
#include "ui_GUIaPL.h"
#include "aPL.h"
#include <qmdiarea.h>
#include <qmdisubwindow.h>
#include <qdebug.h>
#include <waveform.h>


GUIaPL::GUIaPL(QWidget * parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    //Engine test's
    if(engine.RtAudioTest() == 0)
    {
        qDebug() << "RtAudio OK" ;
        engine.RtAudioInit();
    }

    if(engine.RtMidiInTest() == 0)
    {
        qDebug() << "RtMidiIn OK" ;
    }

    if(engine.RtMidiOutTest() == 0)
    {
        qDebug() << "RtMidiOut OK" ;
    }

    initPlots();
    initTableView();

    ui->monitorPlot->addGraph();
    ui->monitorPlot->addGraph();
    ui->monitorPlot->xAxis->setRange(0, 44100);
    ui->monitorPlot->yAxis->setRange(-2, 2);
    //handlerObj->replot();

    /*
    worker1.setWorker(ui->monitorPlot,engine.RtAudioGetWaveForm());
    QObject::connect(&timer1, SIGNAL(timeout()), &worker1, SLOT(hardWork()));
    timer1.start(500);
    timer1.moveToThread(&thread1);
    worker1.moveToThread(&thread1);
    thread1.start();
    */

    restoreControlsValues();
    //lfoUpdate("LFO1");

    /*
    //test do skasowania
    complex<double>* in = new complex<double>[5];
    complex<double>* out = new complex<double>[5];
    in[0].real(1.1231);
    in[1].real(0.1231);
    in[2].real(2.5675);
    in[3].real(5.231);
    in[4].real(1.231);
    //engine.dft(5,in,out);
    qDebug() << "XD fftw" <<out[0].real();
    */

    QPixmap image("wiwav.png");
    ui->wiwav->setPixmap(image);
}

GUIaPL::~GUIaPL()
{
    delete ui;
}

void GUIaPL::restoreControlsValues()
{
    conf.init(configPath);

    ui->filEnvelopeAtkDial->setValue(conf.getInt("filEnvelopeAtkDial"));
    ui->filEnvelopeDecDial->setValue(conf.getInt("filEnvelopeDecDial"));
    ui->filEnvelopeSusDial->setValue(conf.getInt("filEnvelopeSusDial"));
    ui->filEnvelopeRelDial->setValue(conf.getInt("filEnvelopeRelDial"));

    ui->ampEnvelopeAtkDial->setValue(conf.getInt("ampEnvelopeAtkDial"));
    ui->ampEnvelopeDecDial->setValue(conf.getInt("ampEnvelopeDecDial"));
    ui->ampEnvelopeSusDial->setValue(conf.getInt("ampEnvelopeSusDial"));
    ui->ampEnvelopeRelDial->setValue(conf.getInt("ampEnvelopeRelDial"));

    ui->filResoDial->setValue(conf.getInt("filResoDial"));
    ui->filCutOffDial->setValue(conf.getInt("filCutOffDial"));

    ui->lfo1AmountDial->setValue(conf.getInt("lfo1AmountDial"));
    ui->lfo1RateDial->setValue(conf.getInt("lfo1RateDial"));

    lfoRestoreShapeButtonCheched("LFO1");
    lfoRestoreShapeButtonCheched("LFO2");

    ui->lfo2AmountDial->setValue(conf.getInt("lfo2AmountDial"));
    ui->lfo2RateDial->setValue(conf.getInt("lfo2RateDial"));

    ui->osc1CoarseDial->setValue(conf.getInt("osc1CoarseDial"));
    ui->osc1FineDial->setValue(conf.getInt("osc1FineDial"));

    ui->osc2CoarseDial->setValue(conf.getInt("osc2CoarseDial"));
    ui->osc2FineDial->setValue(conf.getInt("osc2FineDial"));

    conf.unmarkChanges();
}

void GUIaPL::on_mainCloseButton_clicked()
{
    this->close();
}

void GUIaPL::on_mainSettingsButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void GUIaPL::on_settingsCloseButton_clicked()
{
    this->close();
}

void GUIaPL::on_settingsBackButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void GUIaPL::initPlots()
{

    initAmpEnvelopePlot();
    initFilterEnvelopePlot();
    initSigPlot();
    ui->aproxLevel->setDigitCount(3);
    ui->phaseLevel->setDigitCount(3);

}

void GUIaPL::initFilterEnvelopePlot()
{

    QVector<double> x(1001), y(1001); // initialize with entries 0..100
    for (int i=0; i<1001; ++i)
    {
        x[i] = i; // x goes from -1 to 1
        y[i] = 0;
    }
    ui->fillEnvPlot->addGraph();
    ui->fillEnvPlot->graph(0)->setData(x,y);

    ui->fillEnvPlot->xAxis->setLabel("Time (miliseconds)");
    ui->fillEnvPlot->yAxis->setLabel("Amplitude");
    // set axes ranges, so we see all data:
    ui->fillEnvPlot->xAxis->setRange(0, 1050);
    ui->fillEnvPlot->yAxis->setRange(0, 1.06);
    ui->fillEnvPlot->replot();
}

void GUIaPL::initSigPlot()
{
    ui->sigPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                 QCP::iSelectLegend | QCP::iSelectPlottables);
    ui->sigPlot->xAxis->setRange(0, 1);
    ui->sigPlot->yAxis->setRange(-1, 1);
    ui->sigPlot->axisRect()->setupFullAxesBox();

    ui->sigPlot->plotLayout()->insertRow(0);
    titleSigPlot = new QCPPlotTitle(ui->sigPlot,"Signal");
    ui->sigPlot->plotLayout()->addElement(0, 0, titleSigPlot);

    ui->sigPlot->xAxis->setLabel("t [s]");
    ui->sigPlot->yAxis->setLabel("A");
    ui->sigPlot->legend->setVisible(false);
    QFont legendFont = font();
    legendFont.setPointSize(8);
    ui->sigPlot->legend->setFont(legendFont);
    ui->sigPlot->legend->setSelectedFont(legendFont);
    ui->sigPlot->legend->setSelectableParts(QCPLegend::spItems);
    ui->sigPlot->addGraph(ui->sigPlot->xAxis,ui->sigPlot->yAxis);
    ui->sigPlot->graph(0)->setPen(QPen(Qt::blue));
    ui->sigPlot->graph(0)->setName("sig");
    ui->sigPlot->replot();

    //fftPlot
    ui->fftPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                 QCP::iSelectLegend | QCP::iSelectPlottables);
    ui->fftPlot->plotLayout()->insertRow(0);

    titleFftPlot = new QCPPlotTitle(ui->fftPlot,"Spectral analisis");
    ui->fftPlot->plotLayout()->addElement(0, 0, titleFftPlot);

    ui->fftPlot->xAxis->setLabel("f [Hz]");
    ui->fftPlot->legend->setVisible(false);
    legendFont.setPointSize(8);
    ui->fftPlot->legend->setFont(legendFont);
    ui->fftPlot->legend->setSelectedFont(legendFont);
    ui->fftPlot->legend->setSelectableParts(QCPLegend::spItems);
    ui->fftPlot->addGraph();
    ui->fftPlot->graph(0)->setPen(QPen(Qt::blue));
    ui->fftPlot->graph(0)->setName("specrum");
    ui->fftPlot->replot();

    //rxyPlot
    ui->rxyPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                 QCP::iSelectLegend | QCP::iSelectPlottables);
    ui->rxyPlot->plotLayout()->insertRow(0);

    titleRxxRxyPlot = new QCPPlotTitle(ui->rxyPlot,"");
    ui->rxyPlot->plotLayout()->addElement(0, 0, titleRxxRxyPlot);
    ui->rxyPlot->legend->setVisible(false);
    legendFont.setPointSize(8);
    ui->rxyPlot->legend->setFont(legendFont);
    ui->rxyPlot->legend->setSelectedFont(legendFont);
    ui->rxyPlot->legend->setSelectableParts(QCPLegend::spItems);
    ui->rxyPlot->addGraph();
    ui->rxyPlot->graph(0)->setPen(QPen(Qt::blue));
    ui->rxyPlot->graph(0)->setName("rxy");
    ui->rxyPlot->replot();

    //opPlot
    ui->opPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                 QCP::iSelectLegend | QCP::iSelectPlottables);
    ui->opPlot->plotLayout()->insertRow(0);
    titleOpPlot = new QCPPlotTitle(ui->opPlot,"");
    ui->opPlot->plotLayout()->addElement(0, 0, titleOpPlot);
    ui->opPlot->legend->setVisible(false);
    legendFont.setPointSize(8);
    ui->opPlot->legend->setFont(legendFont);
    ui->opPlot->legend->setSelectedFont(legendFont);
    ui->opPlot->legend->setSelectableParts(QCPLegend::spItems);
    ui->opPlot->addGraph();
    ui->opPlot->graph(0)->setPen(QPen(Qt::blue));
    ui->opPlot->graph(0)->setName("");
    ui->opPlot->replot();

    //winPlot
    ui->winPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                 QCP::iSelectLegend | QCP::iSelectPlottables);
    ui->winPlot->plotLayout()->insertRow(0);
    _windowing.title = new QCPPlotTitle(ui->winPlot,"XD");
    ui->winPlot->plotLayout()->addElement(0, 0, _windowing.title);
    ui->winPlot->legend->setVisible(false);
    legendFont.setPointSize(8);
    ui->winPlot->legend->setFont(legendFont);
    ui->winPlot->legend->setSelectedFont(legendFont);
    ui->winPlot->legend->setSelectableParts(QCPLegend::spItems);
    ui->winPlot->addGraph();
    ui->winPlot->graph(0)->setPen(QPen(Qt::blue));
    ui->winPlot->graph(0)->setName("");
    ui->winPlot->replot();

}

void GUIaPL::initAmpEnvelopePlot()
{
    QVector<double> x(1001), y(1001); // initialize with entries 0..100
    for (int i=0; i<1001; ++i)
    {
        x[i] = i; // x goes from -1 to 1
        y[i] = 0;
    }
    ui->ampEnvPlot->addGraph();
    ui->ampEnvPlot->graph(0)->setData(x,y);

    ui->ampEnvPlot->xAxis->setLabel("Time (miliseconds)");
    ui->ampEnvPlot->yAxis->setLabel("Amplitude");
    // set axes ranges, so we see all data:
    ui->ampEnvPlot->xAxis->setRange(0, 1050);
    ui->ampEnvPlot->yAxis->setRange(0, 1.06);
    ui->ampEnvPlot->replot();
}

QVector<double> GUIaPL::generateEnvelopePlotValue(int atk, int dec, int sus, int rel)
{
    //  qDebug() << atk << " " << dec << " " << sus << " " << rel;
    QVector<double> value(1001);

    double delta_atk = 0, delta_dec = 0, delta_rel = 0;
    double s = (double)(sus)/100;
    delta_atk = 1 / (atk+0.000001) ;
    delta_dec = (1-s)/(dec+0.000001);
    delta_rel = (s/rel);

    //qDebug() << atk << " " << dec << " " << s << " " << rel;

    // std::cout << a;

    for(int i = 1; i < 1001; i++)
    {
        if(i == 1)
            value[i] = delta_atk;
        if(i <= atk)
        {
            value[i] = value[i-1] + delta_atk;
        }else
            if(i > atk && i <= (atk + dec))
            {
                value[i] = value[i-1] - delta_dec;
            }else
                if( i > (dec + atk) && i <= (1000 - rel))
                {
                    value[i] = s;
                }else
                    if(i > (1000 - rel))
                    {
                        value[i] = value[i-1] - delta_rel;
                    }
    }
    return value;
}


void GUIaPL::repaintFilterEnvelopePlot()
{
    //qDebug() << "inside repaint filer";
    QVector<double> x(1001);
    for (int i=1; i<1001; i++)
    {
        x[i] = i; // x goes from -1 to 1
    }

    //QVector<double> value = filterEnvelopePlotValue(ui->filEnvelopeAtkDial->value(),ui->filEnvelopeDecDial->value(),ui->filEnvelopeSusDial->value(),ui->filEnvelopeRelDial->value());
    QVector<double> value = generateEnvelopePlotValue(
                getDialValue(ui->filEnvelopeAtkDial),
                getDialValue(ui->filEnvelopeDecDial),
                getDialValue(ui->filEnvelopeSusDial),
                getDialValue(ui->filEnvelopeRelDial));

    ui->fillEnvPlot->graph(0)->setData(x,value);
    ui->fillEnvPlot->replot();
}

void GUIaPL::repaintAmpEnvelopePlot()
{
    //qDebug() << "inside repaint filer";
    QVector<double> x(1001);
    for (int i=1; i<1001; i++)
    {
        x[i] = i; // x goes from -1 to 1
    }

    //QVector<double> value = filterEnvelopePlotValue(ui->ampEnvelopeAtkDial->value(),ui->ampEnvelopeDecDial->value(),ui->ampEnvelopeSusDial->value(),ui->ampEnvelopeRelDial->value());
    QVector<double> value = generateEnvelopePlotValue(
                getDialValue(ui->ampEnvelopeAtkDial),
                getDialValue(ui->ampEnvelopeDecDial),
                getDialValue(ui->ampEnvelopeSusDial),
                getDialValue(ui->ampEnvelopeRelDial));

    ui->ampEnvPlot->graph(0)->setData(x,value);
    ui->ampEnvPlot->replot();
}

void GUIaPL::on_filEnvelopeAtkDial_valueChanged(int value)
{
    conf.setInt("filEnvelopeAtkDial",value);
    //qDebug() << value;
    repaintFilterEnvelopePlot();
}

void GUIaPL::on_filEnvelopeDecDial_valueChanged(int value)
{
    conf.setInt("filEnvelopeDecDial",value);
    //qDebug() << value;
    repaintFilterEnvelopePlot();
}

void GUIaPL::on_filEnvelopeSusDial_valueChanged(int value)
{
    conf.setInt("filEnvelopeSusDial",value);
   // qDebug() << value;
    repaintFilterEnvelopePlot();
}

void GUIaPL::on_filEnvelopeRelDial_valueChanged(int value)
{
    conf.setInt("filEnvelopeRelDial",value);
    //qDebug() << value;
    repaintFilterEnvelopePlot();
}

int GUIaPL::getDialValue(QDial *dial)
{
    return dial->value();
}

void GUIaPL::on_ampEnvelopeAtkDial_valueChanged(int value)
{
    conf.setInt("ampEnvelopeAtkDial",value);
    repaintAmpEnvelopePlot();
}

void GUIaPL::on_ampEnvelopeDecDial_valueChanged(int value)
{
    conf.setInt("ampEnvelopeDecDial",value);
    repaintAmpEnvelopePlot();
}

void GUIaPL::on_ampEnvelopeSusDial_valueChanged(int value)
{
    conf.setInt("ampEnvelopeSusDial",value);
    repaintAmpEnvelopePlot();
}

void GUIaPL::on_ampEnvelopeRelDial_valueChanged(int value)
{
    conf.setInt("ampEnvelopeRelDial",value);
    repaintAmpEnvelopePlot();
}

void GUIaPL::refreshMonitor()
{

    float * buffer = engine.RtAudioGetWaveForm();
    QVector <double> L(44101), R(44101),x(44101);

    for(int i = 0; i < 44100 ; i++)
    {
        x[i] = i;
        L[i] = buffer[i*2+0]+1;
        R[i] = buffer[i*2+1]-1;
    }


    ui->monitorPlot->addGraph();
    ui->monitorPlot->graph(0)->setData(x,L);
    ui->monitorPlot->graph(0)->setPen(QPen(Qt::blue));

    ui->monitorPlot->addGraph();
    ui->monitorPlot->graph(1)->setData(x,R);
    ui->monitorPlot->graph(1)->setPen(QPen(Qt::red));
    ui->monitorPlot->xAxis->setRange(0, 44100);
    ui->monitorPlot->yAxis->setRange(-2, 2);
    ui->monitorPlot->setInteraction(QCP::iRangeZoom,true);
    ui->monitorPlot->setInteraction(QCP::iRangeDrag,true);
    ui->monitorPlot->replot();
}

void GUIaPL::on_lfo1AmountDial_valueChanged(int value)
{
    conf.setInt("lfo1AmountDial",value);
    lfoUpdate("LFO1");
}

void GUIaPL::on_lfo1RateDial_valueChanged(int value)
{
    conf.setInt("lfo1RateDial",value);
    lfoUpdate("LFO1");
}

void GUIaPL::on_lfo2AmountDial_valueChanged(int value)
{
    conf.setInt("lfo2AmountDial",value);
    lfoUpdate("LFO2");
}

void GUIaPL::on_lfo2RateDial_valueChanged(int value)
{
    conf.setInt("lfo2RateDial",value);
    lfoUpdate("LFO2");
}

void GUIaPL::on_filResoDial_valueChanged(int value)
{
    conf.setInt("filResoDial",value);
}

void GUIaPL::on_filCutOffDial_valueChanged(int value)
{
    conf.setInt("filCutOffDial",value);
}

void GUIaPL::on_osc1CoarseDial_valueChanged(int value)
{
    conf.setInt("osc1CoarseDial",value);
    oscUpdate("OSC1");
}

void GUIaPL::on_osc1FineDial_valueChanged(int value)
{
    conf.setInt("osc1FineDial",value);
    oscUpdate("OSC1");
}

void GUIaPL::on_osc2CoarseDial_valueChanged(int value)
{
    conf.setInt("osc2CoarseDial",value);
    oscUpdate("OSC2");
}

void GUIaPL::on_osc2FineDial_valueChanged(int value)
{
    conf.setInt("osc2FineDial",value);
    oscUpdate("OSC2");
}

void GUIaPL::on_lfo_1ShapeSaw_clicked()
{
    if(ui->lfo_1ShapeSaw->isChecked())
    {
        conf.setInt("lfo_1Shape",SAW);
    }else conf.setInt("lfo_1Shape",SIN);

    lfoUpdate("LFO1");
    lfoRestoreShapeButtonCheched("LFO1");
}

void GUIaPL::on_lfo_1ShapeSquare_clicked()
{
    if(ui->lfo_1ShapeSquare->isChecked())
    {
        conf.setInt("lfo_1Shape",SQUARE);
    }else conf.setInt("lfo_1Shape",SIN);

    lfoUpdate("LFO1");
    lfoRestoreShapeButtonCheched("LFO1");
}

void GUIaPL::on_lfo_1ShapeSin_clicked()
{
    if(ui->lfo_1ShapeSin->isChecked())
    {
        conf.setInt("lfo_1Shape",SIN);
    }else conf.setInt("lfo_1Shape",SIN);

    lfoUpdate("LFO1");
    lfoRestoreShapeButtonCheched("LFO1");
}

void GUIaPL::on_lfo_1ShapeNoise_clicked()
{
    if(ui->lfo_1ShapeNoise->isChecked())
    {
        conf.setInt("lfo_1Shape",NOISE);
    }else conf.setInt("lfo_1Shape",SIN);

    lfoUpdate("LFO1");
    lfoRestoreShapeButtonCheched("LFO1");
}

void GUIaPL::lfoUpdate(QString LFO_name)
{
    if(QString::compare(LFO_name,"LFO1")==0)
    {
        engine.LFO( 0, //first LFO
                    (float)(conf.getInt("lfo1AmountDial"))/100,
                    (float)(conf.getInt("lfo1RateDial"))/1000,
                    conf.getInt("lfo_1Shape"));
    }

    if(QString::compare(LFO_name,"LFO2")==0)
    {
        engine.LFO( 1, //second LFO
                    (float)(conf.getInt("lfo2AmountDial"))/100,
                    (float)(conf.getInt("lfo2RateDial"))/1000,
                    conf.getInt("lfo_2Shape"));
    }
    engine.updateBuffer();
}

void GUIaPL::oscUpdate(QString OSC_name)
{
    double Q = 2^(1/12);
    if(QString::compare(OSC_name,"OSC1")==0)
    {
        engine.OSC(0,12*Q*((float)(conf.getInt("osc1CoarseDial"))/50),12*Q*((float)(conf.getInt("osc1FineDial"))/100),conf.getInt("osc1_Shape"),conf.getInt("osc1_mix"));
    }

    if(QString::compare(OSC_name,"OSC2")==0)
    {
        engine.OSC(1,12*Q*((float)(conf.getInt("osc2CoarseDial"))/50),Q*((float)(conf.getInt("osc2FineDial"))/100),conf.getInt("osc2_Shape"),conf.getInt("osc2_mix"));
    }
    qDebug()<<conf.getInt("osc1_mix")<<"||"<< conf.getInt("osc2_mix")<<endl;
    engine.updateBuffer();
    refreshMonitor();
}

void GUIaPL::lfoRestoreShapeButtonCheched(QString LFO_name)
{
    if(QString::compare(LFO_name,"LFO1")==0)
    {
        switch(conf.getInt("lfo_1Shape"))
        {
        case NOISE:
        {
            ui->lfo_1ShapeNoise->setChecked(true);
            ui->lfo_1ShapeSaw->setChecked(false);
            ui->lfo_1ShapeSin->setChecked(false);
            ui->lfo_1ShapeSquare->setChecked(false);
        }break;
        case SAW:
        {
            ui->lfo_1ShapeNoise->setChecked(false);
            ui->lfo_1ShapeSaw->setChecked(true);
            ui->lfo_1ShapeSin->setChecked(false);
            ui->lfo_1ShapeSquare->setChecked(false);
        }break;
        case SIN:
        {
            ui->lfo_1ShapeNoise->setChecked(false);
            ui->lfo_1ShapeSaw->setChecked(false);
            ui->lfo_1ShapeSin->setChecked(true);
            ui->lfo_1ShapeSquare->setChecked(false);
        }break;
        case SQUARE:
        {
            ui->lfo_1ShapeNoise->setChecked(false);
            ui->lfo_1ShapeSaw->setChecked(false);
            ui->lfo_1ShapeSin->setChecked(false);
            ui->lfo_1ShapeSquare->setChecked(true);
        }break;
        default:
        {
            ui->lfo_1ShapeSin->setChecked(true);
        }
        }
    }

    if(QString::compare(LFO_name,"LFO2")==0)
    {
        switch(conf.getInt("lfo_2Shape"))
        {
        case NOISE:
        {
            ui->lfo_2ShapeNoise->setChecked(true);
            ui->lfo_2ShapeSaw->setChecked(false);
            ui->lfo_2ShapeSin->setChecked(false);
            ui->lfo_2ShapeSquare->setChecked(false);
        }break;
        case SAW:
        {
            ui->lfo_2ShapeNoise->setChecked(false);
            ui->lfo_2ShapeSaw->setChecked(true);
            ui->lfo_2ShapeSin->setChecked(false);
            ui->lfo_2ShapeSquare->setChecked(false);
        }break;
        case SIN:
        {
            ui->lfo_2ShapeNoise->setChecked(false);
            ui->lfo_2ShapeSaw->setChecked(false);
            ui->lfo_2ShapeSin->setChecked(true);
            ui->lfo_2ShapeSquare->setChecked(false);
        }break;
        case SQUARE:
        {
            ui->lfo_2ShapeNoise->setChecked(false);
            ui->lfo_2ShapeSaw->setChecked(false);
            ui->lfo_2ShapeSin->setChecked(false);
            ui->lfo_2ShapeSquare->setChecked(true);
        }break;
        default:
        {
            ui->lfo_2ShapeSin->setChecked(true);
        }
        }
    }
}

void GUIaPL::on_lfo_2ShapeSaw_clicked()
{
    if(ui->lfo_2ShapeSaw->isChecked())
    {
        conf.setInt("lfo_2Shape",SAW);
    }else conf.setInt("lfo_2Shape",SIN);

    lfoUpdate("LFO2");
    lfoRestoreShapeButtonCheched("LFO2");
}

void GUIaPL::on_lfo_2ShapeSquare_clicked()
{
    if(ui->lfo_2ShapeSquare->isChecked())
    {
        conf.setInt("lfo_2Shape",SQUARE);
    }else conf.setInt("lfo_2Shape",SIN);

    lfoUpdate("LFO2");
    lfoRestoreShapeButtonCheched("LFO2");
}

void GUIaPL::on_lfo_2ShapeSin_clicked()
{
    if(ui->lfo_2ShapeSin->isChecked())
    {
        conf.setInt("lfo_2Shape",SIN);
    }else conf.setInt("lfo_2Shape",SIN);

    lfoUpdate("LFO2");
    lfoRestoreShapeButtonCheched("LFO2");
}

void GUIaPL::on_lfo_2ShapeNoise_clicked()
{
    if(ui->lfo_2ShapeNoise->isChecked())
    {
        conf.setInt("lfo_2Shape",NOISE);
    }else conf.setInt("lfo_2Shape",SIN);

    lfoUpdate("LFO2");
    lfoRestoreShapeButtonCheched("LFO2");
}


void GUIaPL::on_mainBrainButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void GUIaPL::on_algorithmCloseButton_clicked()
{
    this->close();
}

void GUIaPL::on_algorithmBackButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}


void GUIaPL::on_addSignalButton_clicked()
{
    int e = model->rowCount();
    model->setItem(e,0,new QStandardItem(ui->nameToAdd->text()));
    model->setItem(e,1,new QStandardItem(QString::number(ui->freqSpinBox->value())));
    model->setItem(e,2,new QStandardItem(QString::number(ui->sampFreqSpinBox->value())));
    model->setItem(e,3,new QStandardItem(QString::number(ui->ampSpinBox->value())));
    model->setItem(e,4,new QStandardItem(QString::number(ui->SigPhaSeterToAdd->value())));

    ui->listSpectralAnalisis->addItem(ui->nameToAdd->text());
    ui->listCorelationX->addItem(ui->nameToAdd->text());
    ui->listCorelationY->addItem(ui->nameToAdd->text());
    ui->listOperationsA->addItem(ui->nameToAdd->text());
    ui->listOperationsB->addItem(ui->nameToAdd->text());
    ui->listWindowing->addItem(ui->nameToAdd->text());

    if(ui->setSinc->isChecked())
    {
        waveform tmp = waveform(SINC,ui->SigAproxLvL->value(),UNIPOLAR, &engine);
        tmp.fill(
                    ui->sampFreqSpinBox->value(),
                    ui->ampSpinBox->value(),
                    ui->freqSpinBox->value(),
                    ui->SigPhaSeterToAdd->value(),
                    ui->timeSpinBox->value()
                    );
        signalList.push_back(tmp);
    }

    if(ui->setSin->isChecked())
    {
        waveform tmp = waveform(SIN,ui->SigAproxLvL->value(),BIPOLAR, &engine);
        tmp.fill(
                    ui->sampFreqSpinBox->value(),
                    ui->ampSpinBox->value(),
                    ui->freqSpinBox->value(),
                    ui->SigPhaSeterToAdd->value(),
                    ui->timeSpinBox->value()
                    );
        signalList.push_back(tmp);
    }

    if(ui->setSquare->isChecked())
    {
        waveform tmp = waveform(SQUARE,ui->SigAproxLvL->value(),BIPOLAR, &engine);
        tmp.fill(
                    ui->sampFreqSpinBox->value(),
                    ui->ampSpinBox->value(),
                    ui->freqSpinBox->value(),
                    ui->SigPhaSeterToAdd->value(),
                    ui->timeSpinBox->value()
                    );
        signalList.push_back(tmp);
    }

    if(ui->setSqAprox->isChecked())
    {
        waveform tmp = waveform(SQUARE_APROX,ui->SigAproxLvL->value(),BIPOLAR, &engine);
        tmp.fill(
                    ui->sampFreqSpinBox->value(),
                    ui->ampSpinBox->value(),
                    ui->freqSpinBox->value(),
                    ui->SigPhaSeterToAdd->value(),
                    ui->timeSpinBox->value()
                    );
        signalList.push_back(tmp);
    }


    if(ui->setTri->isChecked())
    {
        waveform tmp = waveform(TRIANGLE,ui->SigAproxLvL->value(),BIPOLAR, &engine);
        tmp.fill(
                    ui->sampFreqSpinBox->value(),
                    ui->ampSpinBox->value(),
                    ui->freqSpinBox->value(),
                    ui->SigPhaSeterToAdd->value(),
                    ui->timeSpinBox->value()
                    );
        signalList.push_back(tmp);
    }


    if(ui->setTriAprox->isChecked())
    {
        waveform tmp = waveform(TRIANGLE_APROX,ui->SigAproxLvL->value(),BIPOLAR, &engine);
        tmp.fill(
                    ui->sampFreqSpinBox->value(),
                    ui->ampSpinBox->value(),
                    ui->freqSpinBox->value(),
                    ui->SigPhaSeterToAdd->value(),
                    ui->timeSpinBox->value()
                    );
        signalList.push_back(tmp);
    }


    if(ui->setSaw->isChecked())
    {
        waveform tmp = waveform(SAW,ui->SigAproxLvL->value(),BIPOLAR, &engine);
        tmp.fill(
                    ui->sampFreqSpinBox->value(),
                    ui->ampSpinBox->value(),
                    ui->freqSpinBox->value(),
                    ui->SigPhaSeterToAdd->value(),
                    ui->timeSpinBox->value()
                    );
        signalList.push_back(tmp);
    }


    if(ui->setSawAprox->isChecked())
    {
        waveform tmp = waveform(SAW_APROX,ui->SigAproxLvL->value(),BIPOLAR, &engine);
        tmp.fill(
                    ui->sampFreqSpinBox->value(),
                    ui->ampSpinBox->value(),
                    ui->freqSpinBox->value(),
                    ui->SigPhaSeterToAdd->value(),
                    ui->timeSpinBox->value()
                    );
        signalList.push_back(tmp);
    }

    QVector<double> x(signalList.at(e).N), y(signalList.at(e).N);
    for (int n=0; n<signalList.at(e).N; n++)
    {
        x[n] = signalList.at(e).timeField.at(n);
        y[n] = signalList.at(e).collection->at(n).real();
    }
    ui->sigPlot->graph(0)->setData(x,y);
    ui->sigPlot->replot();
}

void GUIaPL::initTableView()
{
    model = new QStandardItemModel(0,3,this); //2 Rows and 3 Columns
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Name")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("f[Hz]")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("fs[Hz]")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("A")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Phase")));
    ui->tableView->setModel(model);
}

void GUIaPL::on_tableView_clicked(const QModelIndex &index)
{
    int e = index.row();
    int shape = signalList.at(e).shape;
    QVector<double> x(signalList.at(e).N), y(signalList.at(e).N);
    double max=0, min=0;

    for (int n=0; n<signalList.at(e).N; n++)
    {
        if(shape==FFT_SHAPE || shape==IFFT_SHAPE)
        {
            x[n] = n;
            y[n] = abs(signalList.at(e).collection->at(n));
        }else{
            x[n] = signalList.at(e).timeField.at(n);
            y[n] = signalList.at(e).collection->at(n).real();
        }
        max=(max<y[n])?(y[n]):(max);
        min=(min>y[n])?(y[n]):(min);
    }

    if(shape==FFT_SHAPE || shape==IFFT_SHAPE)
    {
        titleSigPlot->setText("Spectral analisis");
        ui->sigPlot->xAxis->setLabel("f [Hz]");
        ui->sigPlot->yAxis->setLabel("");
        ui->sigPlot->xAxis->setRange(0, signalList.at(e).N);
    }else{
        titleSigPlot->setText("Signal preview");
        ui->sigPlot->xAxis->setLabel("t [s]");
        ui->sigPlot->yAxis->setLabel("A");
        ui->sigPlot->xAxis->setRange(0, signalList.at(e).t);
    }

    ui->sigPlot->yAxis->setRange(min, max);
    ui->sigPlot->addGraph(ui->sigPlot->xAxis,ui->sigPlot->yAxis);
    ui->sigPlot->axisRect()->setupFullAxesBox();
    ui->sigPlot->graph(0)->setData(x,y);
    ui->sigPlot->replot();
}

void GUIaPL::on_SigAproxLvL_valueChanged(int value)
{
    ui->aproxLevel->display(QString::number(value));
}

void GUIaPL::on_SigPhaSeterToAdd_valueChanged(int value)
{
    ui->phaseLevel->display(QString::number(value));
}

void GUIaPL::on_deleteSelected_3_clicked()
{
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();

    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = indexes.at(i);
        model->removeRow(index.row());
        signalList.removeAt(index.row());
    }

    //Dorobić usówanie z list
    /*
    ui->listSpectralAnalisis
    ui->listCorelationX
    ui->listCorelationY
    ui->listOperationsA
    ui->listOperationsB
    ui->listWindowing
    */
}

void GUIaPL::on_catchFFTButton_clicked()
{
    lastSpectralOperation = FFT_SHAPE;
    int e = signalIndexToSpectralAnalisis;
    int N = signalList.at(e).N;
    qDebug() <<"Liczba próbek w sygnale"<<N;
    tmpSpectralSignal = waveform(FFT_SHAPE,0,UNKNOWNPOLAR, &engine);
    tmpSpectralSignal.fs = signalList.at(signalIndexToSpectralAnalisis).fs;
    tmpSpectralSignal.fi = signalList.at(signalIndexToSpectralAnalisis).fi;
    tmpSpectralSignal.t = signalList.at(signalIndexToSpectralAnalisis).t;
    tmpSpectralSignal.A = signalList.at(signalIndexToSpectralAnalisis).A;
    engine.fft(N, signalList.at(signalIndexToSpectralAnalisis).collection, tmpSpectralSignal.collection );
    tmpSpectralSignal.N=N;

    QVector<double> x(N), y(N);
    double max=0;
    for (int n=0; n<N; n++)
    {
        x[n] = n;
        y[n] = abs(tmpSpectralSignal.collection->at(n));
        max=(max<y[n])?(y[n]):(max);
    }

    titleFftPlot->setText("Spectral analisis (FFT)");
    ui->fftPlot->xAxis->setLabel("f [Hz]");
    ui->fftPlot->yAxis->setLabel("");
    ui->fftPlot->xAxis->setRange(0, N);
    ui->fftPlot->yAxis->setRange(-0.001, max);
    ui->fftPlot->addGraph(ui->fftPlot->xAxis,ui->fftPlot->yAxis);
    ui->fftPlot->axisRect()->setupFullAxesBox();
    ui->fftPlot->graph(0)->setData(x,y);
    ui->fftPlot->replot();
}

void GUIaPL::on_catchIFFTButton_clicked()
{
    lastSpectralOperation = IFFT_SHAPE;
    int e = signalIndexToSpectralAnalisis;
    int N = signalList.at(e).N;
    qDebug() <<"Liczba próbek w sygnale"<<N;
    tmpSpectralSignal = waveform(IFFT_SHAPE,0,UNKNOWNPOLAR, &engine);
    tmpSpectralSignal.fs = signalList.at(signalIndexToSpectralAnalisis).fs;
    tmpSpectralSignal.fi = signalList.at(signalIndexToSpectralAnalisis).fi;
    tmpSpectralSignal.t = signalList.at(signalIndexToSpectralAnalisis).t;
    tmpSpectralSignal.A = signalList.at(signalIndexToSpectralAnalisis).A;
    engine.ifft(N, signalList.at(signalIndexToSpectralAnalisis).collection, tmpSpectralSignal.collection );
    tmpSpectralSignal.N=N;

    QVector<double> x(N), y(N);
    double max=0, min=0;
    double dt=1/signalList.at(signalIndexToSpectralAnalisis).fs;
    for (int n=0; n<N; n++)
    {
        x[n] = dt*n;
        y[n] = tmpSpectralSignal.collection->at(n).real();
        max=(max<y[n])?(y[n]):(max);
        min=(min>y[n])?(y[n]):(min);
    }

    titleFftPlot->setText("Spectral analisis (IFFT)");
    ui->fftPlot->xAxis->setLabel("t [s]");
    ui->fftPlot->yAxis->setLabel("A");
    ui->fftPlot->xAxis->setRange(0, signalList.at(signalIndexToSpectralAnalisis).t);
    ui->fftPlot->yAxis->setRange(min, max);
    ui->fftPlot->addGraph(ui->fftPlot->xAxis,ui->fftPlot->yAxis);
    ui->fftPlot->axisRect()->setupFullAxesBox();
    ui->fftPlot->graph(0)->setData(x,y);
    ui->fftPlot->replot();
}

void GUIaPL::on_listSpectralAnalisis_clicked(const QModelIndex &index)
{
    signalIndexToSpectralAnalisis = index.row();
    int shape = signalList.at(signalIndexToSpectralAnalisis).shape;

    QVector<double> x(signalList.at(signalIndexToSpectralAnalisis).N),
                    y(signalList.at(signalIndexToSpectralAnalisis).N);
    double max=0, min=0;

    double dt = 1 / signalList.at(signalIndexToSpectralAnalisis).fs;
    for (int n=0; n<signalList.at(signalIndexToSpectralAnalisis).N; n++)
    {
        switch(shape){
            case FFT_SHAPE:
            {
                x[n] = n;
                y[n] = abs(signalList.at(signalIndexToSpectralAnalisis).collection->at(n));
            }break;
            case IFFT_SHAPE:
            {
                x[n] = dt*n;
                y[n] = signalList.at(signalIndexToSpectralAnalisis).collection->at(n).real();
            }break;
            default:
            {
                x[n] = signalList.at(signalIndexToSpectralAnalisis).timeField.at(n);
                y[n] = signalList.at(signalIndexToSpectralAnalisis).collection->at(n).real();
            }
        }
        max=(max<y[n])?(y[n]):(max);
        min=(min>y[n])?(y[n]):(min);
    }

    switch(shape){
        case FFT_SHAPE:
        {
            titleFftPlot->setText("Spectral analisis");
            ui->fftPlot->xAxis->setLabel("f [Hz]");
            ui->fftPlot->yAxis->setLabel("");
            ui->fftPlot->xAxis->setRange(0, signalList.at(signalIndexToSpectralAnalisis).N);
        }break;
        case IFFT_SHAPE:
        default:
        {
            titleFftPlot->setText("Signal preview");
            ui->fftPlot->xAxis->setLabel("t [s]");
            ui->fftPlot->yAxis->setLabel("A");
            ui->fftPlot->xAxis->setRange(0, signalList.at(signalIndexToSpectralAnalisis).t);
        }
    }

    ui->fftPlot->yAxis->setRange(min, max);
    ui->fftPlot->addGraph(ui->fftPlot->xAxis,ui->fftPlot->yAxis);
    ui->fftPlot->axisRect()->setupFullAxesBox();
    ui->fftPlot->graph(0)->setData(x,y);
    ui->fftPlot->replot();
}

void GUIaPL::on_catchSpectral_clicked()
{
    //asdasdas
    signalList.push_back(tmpSpectralSignal);
    QString name = ui->listSpectralAnalisis->item(signalIndexToSpectralAnalisis)->text();
    if(lastSpectralOperation==FFT_SHAPE){
        name = "FFT(" + name +")";
    }else{
        name = "IFFT(" + name +")";
    }
    int e = model->rowCount();
    model->setItem(e,0,new QStandardItem(name));
    model->setItem(e,1,new QStandardItem("---"));
    model->setItem(e,2,new QStandardItem("---"));
    model->setItem(e,3,new QStandardItem("---"));
    model->setItem(e,4,new QStandardItem("---"));

    ui->listSpectralAnalisis->addItem(name);
    ui->listCorelationX->addItem(name);
    ui->listCorelationY->addItem(name);
    ui->listOperationsA->addItem(name);
    ui->listOperationsB->addItem(name);
    ui->listWindowing->addItem(name);
}

void GUIaPL::on_listCorelationX_clicked(const QModelIndex &index)
{
    signalIndexXToRxxRxy = index.row();
    int shape = signalList.at(signalIndexXToRxxRxy).shape;


    QVector<double> x(signalList.at(signalIndexXToRxxRxy).N),
                    y(signalList.at(signalIndexXToRxxRxy).N);
    double max=0, min=0;
    double dt = 1 / signalList.at(signalIndexXToRxxRxy).fs;

    for (int n=0; n<signalList.at(signalIndexXToRxxRxy).N; n++)
    {
        switch(shape){
            case FFT_SHAPE:
            {
                x[n] = n;
                y[n] = abs(signalList.at(signalIndexXToRxxRxy).collection->at(n));
            }break;
            case IFFT_SHAPE:
            {
                x[n] = dt*n;
                y[n] = signalList.at(signalIndexXToRxxRxy).collection->at(n).real();
            }break;
            default:
            {
                x[n] = signalList.at(signalIndexXToRxxRxy).timeField.at(n);
                y[n] = signalList.at(signalIndexXToRxxRxy).collection->at(n).real();
            }
        }
       max=(max<y[n])?(y[n]):(max);
       min=(min>y[n])?(y[n]):(min);
    }

    switch(shape){
        case FFT_SHAPE:
        {
            titleRxxRxyPlot->setText("Spectral analisis");
            ui->rxyPlot->xAxis->setLabel("f [Hz]");
            ui->rxyPlot->yAxis->setLabel("");
            ui->rxyPlot->xAxis->setRange(0, signalList.at(signalIndexXToRxxRxy).N);
        }break;
        case IFFT_SHAPE:
        default:
        {
            titleRxxRxyPlot->setText("Signal preview");
            ui->rxyPlot->xAxis->setLabel("t [s]");
            ui->rxyPlot->yAxis->setLabel("A");
            ui->rxyPlot->xAxis->setRange(0, signalList.at(signalIndexXToRxxRxy).t);
        }
    }

    ui->rxyPlot->yAxis->setRange(min, max);
    ui->rxyPlot->addGraph(ui->rxyPlot->xAxis,ui->rxyPlot->yAxis);
    ui->rxyPlot->axisRect()->setupFullAxesBox();
    ui->rxyPlot->graph(0)->setData(x,y);
    ui->rxyPlot->replot();
}

void GUIaPL::on_listCorelationY_clicked(const QModelIndex &index)
{
    signalIndexYToRxxRxy = index.row();
    int shape = signalList.at(signalIndexYToRxxRxy).shape;


    QVector<double> x(signalList.at(signalIndexYToRxxRxy).N),
                    y(signalList.at(signalIndexYToRxxRxy).N);
    double max=0, min=0;
    double dt = 1 / signalList.at(signalIndexYToRxxRxy).fs;

    for (int n=0; n<signalList.at(signalIndexYToRxxRxy).N; n++)
    {
        switch(shape){
            case FFT_SHAPE:
            {
                x[n] = n;
                y[n] = abs(signalList.at(signalIndexYToRxxRxy).collection->at(n));
            }break;
            case IFFT_SHAPE:
            {
                x[n] = dt*n;
                y[n] = signalList.at(signalIndexYToRxxRxy).collection->at(n).real();
            }break;
            default:
            {
                x[n] = signalList.at(signalIndexYToRxxRxy).timeField.at(n);
                y[n] = signalList.at(signalIndexYToRxxRxy).collection->at(n).real();
            }
        }
       max=(max<y[n])?(y[n]):(max);
       min=(min>y[n])?(y[n]):(min);
    }

    switch(shape){
        case FFT_SHAPE:
        {
            titleRxxRxyPlot->setText("Spectral analisis");
            ui->rxyPlot->xAxis->setLabel("f [Hz]");
            ui->rxyPlot->yAxis->setLabel("");
            ui->rxyPlot->xAxis->setRange(0, signalList.at(signalIndexYToRxxRxy).N);
        }break;
        case IFFT_SHAPE:
        default:
        {
            titleRxxRxyPlot->setText("Signal preview");
            ui->rxyPlot->xAxis->setLabel("t [s]");
            ui->rxyPlot->yAxis->setLabel("A");
            ui->rxyPlot->xAxis->setRange(0, signalList.at(signalIndexYToRxxRxy).t);
        }
    }

    ui->rxyPlot->yAxis->setRange(min, max);
    ui->rxyPlot->addGraph(ui->rxyPlot->xAxis,ui->rxyPlot->yAxis);
    ui->rxyPlot->axisRect()->setupFullAxesBox();
    ui->rxyPlot->graph(0)->setData(x,y);
    ui->rxyPlot->replot();
}

void GUIaPL::on_rxyCheck_clicked()
{
    vector< complex<double> > * rxy = engine.rxy(signalList.at(signalIndexXToRxxRxy).collection,
                                                 signalList.at(signalIndexYToRxxRxy).collection);

    int N = rxy->size();
    QVector<double> x(N),
                    y(N);
    double max=0, min=0;
    for(int n=0;n<N;n++)
    {
        x[n]=n-(N/2);
        y[n]=rxy->at(n).real();
        max=(max<y[n])?(y[n]):(max);
        min=(min>y[n])?(y[n]):(min);
    }


    switch(signalIndexXToRxxRxy-signalIndexYToRxxRxy){
        case 0:
        {
            QString nameX = ui->listCorelationX->item(signalIndexXToRxxRxy)->text();
            titleRxxRxyPlot->setText("Rxx( "+nameX+" )");
        }break;
        default:
        {
            QString nameX = ui->listCorelationX->item(signalIndexXToRxxRxy)->text();
            QString nameY = ui->listCorelationY->item(signalIndexYToRxxRxy)->text();
            titleRxxRxyPlot->setText("Rxy( "+nameX+", "+nameY+" )");
        }
    }

    ui->rxyPlot->xAxis->setLabel("T (lag)");
    ui->rxyPlot->yAxis->setLabel("");
    ui->rxyPlot->xAxis->setRange(-N/2, N/2);
    ui->rxyPlot->yAxis->setRange(min, max);
    ui->rxyPlot->graph(0)->setData(x,y);
    ui->rxyPlot->replot();
}

void GUIaPL::on_listOperationsA_clicked(const QModelIndex &index)
{
    signalIndexOperationsA = index.row();
}

void GUIaPL::on_listOperationsB_clicked(const QModelIndex &index)
{
    signalIndexOperationsB = index.row();
}

void GUIaPL::on_additionSignal_clicked()
{
    performArithmeticOperation(ADD);
}

void GUIaPL::on_subtractionSignal_clicked()
{
    performArithmeticOperation(SUB);
}

void GUIaPL::on_multiplicationSignal_clicked()
{
    performArithmeticOperation(MUL);
}

void GUIaPL::on_divisionSignal_clicked()
{
    performArithmeticOperation(DIV);
}

void  GUIaPL::performArithmeticOperation(int typeOperations)
{

    int a = signalIndexOperationsA;
    int b = signalIndexOperationsB;

    tmpOperationSignal = waveform(UNKNOWN_SHAPE,0,UNKNOWNPOLAR, &engine);
    switch (typeOperations) {
        case ADD:{
            tmpOperationSignal.collection = engine.add(signalList.at(a).collection, signalList.at(b).collection );
        }break;

        case SUB:{
            tmpOperationSignal.collection = engine.sub(signalList.at(a).collection, signalList.at(b).collection );
        }break;

        case MUL:{
            tmpOperationSignal.collection = engine.mul(signalList.at(a).collection, signalList.at(b).collection );
        }break;

        case DIV:{
            tmpOperationSignal.collection = engine.div(signalList.at(a).collection, signalList.at(b).collection );
        }break;
        default:break;
    }
    tmpOperationSignal.N=tmpOperationSignal.collection->size();

    QVector<double> x(tmpOperationSignal.N),
                    y(tmpOperationSignal.N);
    double max=0, min=0;
    for(int n=0;n<tmpOperationSignal.N;n++)
    {
        x[n]=n;
        y[n]=tmpOperationSignal.collection->at(n).real();
        max=(max<y[n])?(y[n]):(max);
        min=(min>y[n])?(y[n]):(min);
    }

    QString nameA = ui->listOperationsA->item(signalIndexOperationsA)->text();
    QString nameB = ui->listOperationsB->item(signalIndexOperationsB)->text();
    titleOpPlot->setText("Op( "+nameA+", "+nameB+" )");

    ui->opPlot->xAxis->setLabel("");
    ui->opPlot->yAxis->setLabel("");
    ui->opPlot->xAxis->setRange(0, tmpOperationSignal.N);
    ui->opPlot->yAxis->setRange(min, max);
    ui->opPlot->graph(0)->setData(x,y);
    ui->opPlot->replot();

}

void GUIaPL::on_listWindowing_clicked(const QModelIndex &index)
{
    _windowing.signalIndex = index.row();
}

void GUIaPL::performWindowingOperation(int typeWindow)
{
    _windowing.lastAction = typeWindow;
    _windowing.wForm = waveform(UNKNOWN_SHAPE,0,UNKNOWNPOLAR, &engine);
    _windowing.wForm.collection = engine.windowing(signalList.at(_windowing.signalIndex).collection, typeWindow );
    _windowing.wForm.N=_windowing.wForm.collection->size();

    QVector<double> x(_windowing.wForm.N),
                    y(_windowing.wForm.N);
    double max=0, min=0;
    for(int n=0;n<_windowing.wForm.N;n++)
    {
        x[n]=n;
        y[n]=_windowing.wForm.collection->at(n).real();
        max=(max<y[n])?(y[n]):(max);
        min=(min>y[n])?(y[n]):(min);
    }

    QString name = ui->listWindowing->item(_windowing.signalIndex)->text();
    _windowing.title->setText("W( "+name+" )");
    ui->winPlot->xAxis->setLabel("");
    ui->winPlot->yAxis->setLabel("");
    ui->winPlot->xAxis->setRange(0, _windowing.wForm.N);
    ui->winPlot->yAxis->setRange(min, max);
    ui->winPlot->graph(0)->setData(x,y);
    ui->winPlot->replot();

}

void GUIaPL::on_windowingHamming_clicked()
{
    performWindowingOperation(HAMMING);
}

void GUIaPL::on_windowingHanning_clicked()
{
    performWindowingOperation(HANNING);
}

void GUIaPL::on_windowingBlackman_clicked()
{
    performWindowingOperation(BLACKMAN);
}

void GUIaPL::on_windowingBlackmanHarris_clicked()
{
    performWindowingOperation(BLACKMAN_HARRIS);
}

void GUIaPL::on_windowingBarlettHanning_clicked()
{
    performWindowingOperation(BARLET_HANNING);
}

void GUIaPL::on_osc1Saw_clicked()
{
    if(ui->osc1Saw->isChecked())
    {
        conf.setInt("osc1_Shape",SAW);
    }else conf.setInt("osc1_Shape",SIN);

    oscUpdate("OSC1");
    oscRestoreShapeButtonCheched("OSC1");
}

void GUIaPL::oscRestoreShapeButtonCheched(QString OSC_name)
{
    if(QString::compare(OSC_name,"OSC1")==0)
    {
        switch(conf.getInt("osc1_Shape"))
        {
        case NOISE:
        {
            ui->osc1Nois->setChecked(true);
            ui->osc1Saw->setChecked(false);
            ui->osc1Sin->setChecked(false);
            ui->osc1Squa->setChecked(false);
        }break;
        case SAW:
        {
            ui->osc1Nois->setChecked(false);
            ui->osc1Saw->setChecked(true);
            ui->osc1Sin->setChecked(false);
            ui->osc1Squa->setChecked(false);
        }break;
        case SIN:
        {
            ui->osc1Nois->setChecked(false);
            ui->osc1Saw->setChecked(false);
            ui->osc1Sin->setChecked(true);
            ui->osc1Squa->setChecked(false);
        }break;
        case SQUARE:
        {
            ui->osc1Nois->setChecked(false);
            ui->osc1Saw->setChecked(false);
            ui->osc1Sin->setChecked(false);
            ui->osc1Squa->setChecked(true);
        }break;
        default:
        {
            ui->osc1Sin->setChecked(true);
        }
        }
    }

    if(QString::compare(OSC_name,"OSC2")==0)
    {
        switch(conf.getInt("osc2_Shape"))
        {
        case NOISE:
        {
            ui->osc2Nois->setChecked(true);
            ui->osc2Saw->setChecked(false);
            ui->osc2Sin->setChecked(false);
            ui->osc2Squa->setChecked(false);
        }break;
        case SAW:
        {
            ui->osc2Nois->setChecked(false);
            ui->osc2Saw->setChecked(true);
            ui->osc2Sin->setChecked(false);
            ui->osc2Squa->setChecked(false);
        }break;
        case SIN:
        {
            ui->osc2Nois->setChecked(false);
            ui->osc2Saw->setChecked(false);
            ui->osc2Sin->setChecked(true);
            ui->osc2Squa->setChecked(false);
        }break;
        case SQUARE:
        {
            ui->osc2Nois->setChecked(false);
            ui->osc2Saw->setChecked(false);
            ui->osc2Sin->setChecked(false);
            ui->osc2Squa->setChecked(true);
        }break;
        default:
        {
            ui->osc2Sin->setChecked(true);
        }
        }
    }
}
void GUIaPL::on_osc1Squa_clicked()
{

    if(ui->osc1Squa->isChecked())
    {
        conf.setInt("osc1_Shape",SQUARE);
    }else conf.setInt("osc1_Shape",SIN);

    oscUpdate("OSC1");
    oscRestoreShapeButtonCheched("OSC1");
}

void GUIaPL::on_osc1Sin_clicked()
{
    if(ui->osc1Sin->isChecked())
    {
        conf.setInt("osc1_Shape",SIN);
    }else conf.setInt("osc1_Shape",SIN);

    oscUpdate("OSC1");
    oscRestoreShapeButtonCheched("OSC1");
}

void GUIaPL::on_osc1Nois_clicked()
{
    if(ui->osc1Nois->isChecked())
    {
        conf.setInt("osc1_Shape",NOISE);
    }else conf.setInt("osc1_Shape",SIN);

    oscUpdate("OSC1");
    oscRestoreShapeButtonCheched("OSC1");
}

void GUIaPL::on_osc2Saw_clicked()
{
    if(ui->osc2Saw->isChecked())
    {
        conf.setInt("osc2_Shape",SAW);
    }else conf.setInt("osc2_Shape",SIN);

    oscUpdate("OSC2");
    oscRestoreShapeButtonCheched("OSC2");
}

void GUIaPL::on_osc2Squa_clicked()
{
    if(ui->osc2Squa->isChecked())
    {
        conf.setInt("osc2_Shape",SQUARE);
    }else conf.setInt("osc2_Shape",SIN);

    oscUpdate("OSC2");
    oscRestoreShapeButtonCheched("OSC2");
}

void GUIaPL::on_osc2Sin_clicked()
{
    if(ui->osc2Sin->isChecked())
    {
        conf.setInt("osc2_Shape",SIN);
    }else conf.setInt("osc2_Shape",SIN);

    oscUpdate("OSC2");
    oscRestoreShapeButtonCheched("OSC2");
}

void GUIaPL::on_osc2Nois_clicked()
{
    if(ui->osc2Nois->isChecked())
    {
        conf.setInt("osc2_Shape",NOISE);
    }else conf.setInt("osc2_Shape",SIN);

    oscUpdate("OSC2");
    oscRestoreShapeButtonCheched("OSC2");
}


void GUIaPL::on_keyC_clicked()
{
    engine.TRIG_KEY(261.625568);
    refreshMonitor();
}

void GUIaPL::on_keyD_clicked()
{
    engine.TRIG_KEY(293.664771);
    refreshMonitor();
}

void GUIaPL::on_keyE_clicked()
{
    engine.TRIG_KEY(329.627560);
    refreshMonitor();
}

void GUIaPL::on_keyF_clicked()
{
    engine.TRIG_KEY(349.228235);
    refreshMonitor();
}

void GUIaPL::on_keyG_clicked()
{
    engine.TRIG_KEY(391.995440);
    refreshMonitor();
}

void GUIaPL::on_keyA_clicked()
{
    engine.TRIG_KEY(440.000005);
    refreshMonitor();
}

void GUIaPL::on_keyH_clicked()
{
    engine.TRIG_KEY(493.883306);
    refreshMonitor();
}

void GUIaPL::on_oscSlider_valueChanged(int value)
{
    conf.setInt("osc1_mix",100-value);
    conf.setInt("osc2_mix",value);
    oscUpdate("OSC1");
    oscUpdate("OSC2");
}
